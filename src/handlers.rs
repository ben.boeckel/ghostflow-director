// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Handlers for jobs for webhooks from hosting services.

use std::sync::Arc;

use json_job_dispatch::Handler;
use serde_json::Value;
use thiserror::Error;

use crate::ghostflow_ext::DirectorHostingService;

pub mod common;
mod github;
mod gitlab;
#[cfg(test)]
pub mod test;

use crate::config::Host;

/// Errors which can occur when constructing a hosting service.
#[derive(Debug, Error)]
pub enum HostError {
    /// Failure to construct a GitLab service.
    #[error("gitlab error: {}", source)]
    Gitlab {
        /// The source of the error.
        #[from]
        source: gitlab::GitlabHostError,
    },
    /// Failure to construct a GitHub service.
    #[error("github error: {}", source)]
    Github {
        /// The source of the error.
        #[from]
        source: github::GithubHostError,
    },
    /// An unknown API was given.
    #[error("unknown api `{}`", api)]
    UnknownApi {
        /// The unknown API.
        api: String,
    },
}

impl HostError {
    fn unknown_api(api: String) -> Self {
        HostError::UnknownApi {
            api,
        }
    }
}

/// A structure containing all the information necessary to communicate with a service.
pub struct HostHandler {
    /// The `HostingService` to use for communicating with the service.
    pub service: Arc<dyn DirectorHostingService>,
    /// The handler to use for handling webhooks.
    pub handler: Box<dyn FnOnce(Host) -> Box<dyn Handler>>,
}

impl HostHandler {
    /// Connect to a `HostingService`.
    pub fn new(
        api: &str,
        url: &Option<String>,
        secrets: &Value,
        name: String,
    ) -> Result<Self, HostError> {
        match api {
            "gitlab" => Ok(gitlab::host_handler(url, secrets, name)?),
            "github" => Ok(github::host_handler(url, secrets, name)?),
            _ => Err(HostError::unknown_api(api.into())),
        }
    }
}
