// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::BTreeMap;
use std::fmt;
use std::sync::Arc;

use ghostflow::actions::merge;
use ghostflow::host::{
    HostedPipelineService, HostedProject, HostingServiceError, MergeRequest, PipelineState, User,
};
use ghostflow::utils::Trailer;
use git_workarea::Identity;
use log::{info, warn};
use sha2::{Digest, Sha256};
use thiserror::Error;

use crate::config::io;

/// The salt for ghostflow-director-based bypass tokens.
const GHOSTFLOW_BYPASS_SALT: &[u8] = b"ghostflow-director-bypass-token";
/// Prefix for bypass tokens.
const GHOSTFLOW_BYPASS_PREFIX: &str = "gdbt-";
/// The trailer which contains the bypass token.
const BYPASS_TRAILER_TOKEN: &str = "Bypass-token";

#[derive(Debug, Clone)]
struct CiBypassInformation {
    /// The expected token.
    expected: Option<String>,
    /// The actual token given.
    actual: Option<String>,
    /// Tripwire in case of an error.
    errors: Vec<String>,
}

impl CiBypassInformation {
    fn check(&mut self, policy_errors: &mut Vec<String>) {
        // Not using constant-time comparison here. If this timing can be pulled from the noise of
        // the network activity inherent in the codepaths that get here, then we can worry about
        // it. Until then, let it be.
        if self.expected == self.actual {
            // Only clear if there is an expected token.
            if self.expected.is_some() {
                policy_errors.clear();
            }
        } else if self.actual.is_some() {
            self.errors.push("Invalid bypass token provided".into());
        }
    }

    fn add_token(&mut self, token: String) {
        if self.actual.replace(token).is_some() {
            self.errors
                .push("Multiple bypass tokens provided; at most one is permitted".into());
        }
    }
}

/// The filter for the workflow policy.
#[derive(Debug, Clone)]
pub struct WorkflowMergePolicyFilter {
    /// The trailers for the merge commit message.
    trailers: Vec<Trailer>,
    /// Errors which have occurred due to trailer processing.
    errors: Vec<String>,
    /// Errors which have occurred due to the CI policy.
    ci_policy_errors: Vec<String>,
    /// Information about bypassing the CI policy.
    bypass_info: CiBypassInformation,
}

impl merge::MergePolicyFilter for WorkflowMergePolicyFilter {
    fn process_trailer(&mut self, trailer: &Trailer, _: Option<&User>) {
        if trailer.token == BYPASS_TRAILER_TOKEN {
            self.bypass_info.add_token(trailer.value.clone());
            return;
        }

        // Ignore trailers without a `-by` suffix.
        if !trailer.token.ends_with("-by") {
            return;
        }

        // TODO: Allow only a whitelist of trailers.
        // TODO: Require at least an Acked-by (with sufficient permissions).
        // TODO: Require Reviewed-by (with sufficient permissions/assignee(s)).
        // TODO: Look at Rejected-by.

        // Accept the trailer.
        self.trailers.push(trailer.clone());
    }

    fn result(mut self) -> Result<Vec<Trailer>, Vec<String>> {
        // Check the bypass information.
        self.bypass_info.check(&mut self.ci_policy_errors);

        // Turn all bypass token errors into normal errors.
        self.errors.extend(self.bypass_info.errors);

        // Turn all CI errors into normal errors.
        self.errors.extend(self.ci_policy_errors);

        // Return the errors or trailers.
        if self.errors.is_empty() {
            Ok(self.trailers)
        } else {
            Err(self.errors)
        }
    }
}

/// Errors which may occur for merge policies.
#[derive(Debug, Error)]
pub enum WorkflowMergePolicyError {
    /// No pipeline services are available.
    #[error("no pipeline services are available")]
    NoPipeline {},
    /// Failure to determine the CI user information.
    #[error("failed to determine CI user: {}", source)]
    UnknownCiUser {
        /// The hosting service error.
        #[source]
        source: HostingServiceError,
    },
}

impl WorkflowMergePolicyError {
    fn no_pipeline() -> Self {
        Self::NoPipeline {}
    }

    fn unknown_ci_user(source: HostingServiceError) -> Self {
        Self::UnknownCiUser {
            source,
        }
    }
}

type WorkflowMergePolicyResult<T> = Result<T, WorkflowMergePolicyError>;

/// The policy structure for merging.
#[derive(Clone)]
pub struct CiWorkflowMergePolicy {
    /// The merge policy for the branch.
    conf: io::CiPipelinePolicy,
    /// The service.
    service: Arc<dyn HostedPipelineService>,
    /// The identity of the user.
    identity: Identity,
}

impl fmt::Debug for CiWorkflowMergePolicy {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("CiWorkflowMergePolicy")
            .field("conf", &self.conf)
            .field("identity", &self.identity)
            .finish()
    }
}

/// The policy structure for merging.
#[derive(Debug, Clone)]
pub struct WorkflowMergePolicy {
    /// The CI policy for merging.
    ci_policy: Option<CiWorkflowMergePolicy>,
    /// The identity of the director.
    identity: Identity,
    /// The secret to allow bypassing CI policy checks.
    bypass_secret: Option<String>,
}

impl WorkflowMergePolicy {
    /// Create a new merge policy.
    pub(in crate::config) fn new(
        conf: io::MergePolicy,
        project: HostedProject,
        identity: Identity,
    ) -> WorkflowMergePolicyResult<Self> {
        let HostedProject {
            name,
            service,
        } = project;
        let io::MergePolicy {
            ci_pipeline,
            bypass_secret,
        } = conf;

        let ci_policy = if let Some(ci_pipeline) = ci_pipeline {
            let service = service
                .as_pipeline_service()
                .ok_or_else(WorkflowMergePolicyError::no_pipeline)?;

            let user = service
                .user(&name, &ci_pipeline.ci_user)
                .map_err(WorkflowMergePolicyError::unknown_ci_user)?;

            let identity = Identity {
                name: user.name,
                email: user.email,
            };

            Some(CiWorkflowMergePolicy {
                conf: ci_pipeline,
                service,
                identity,
            })
        } else {
            None
        };

        Ok(WorkflowMergePolicy {
            ci_policy,
            identity,
            bypass_secret,
        })
    }
}

impl io::CiReviewAction {
    fn trailer(self) -> Option<&'static str> {
        match self {
            Self::Ack => Some("Acked-by"),
            Self::Review => Some("Reviewed-by"),
            Self::Tested => Some("Tested-by"),
            Self::Reject => Some("Rejected-by"),
            Self::Ignore => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum StageState {
    Missing,
    Failing,
    Incomplete,
    Passing,
}

impl Default for StageState {
    fn default() -> Self {
        Self::Missing
    }
}

impl StageState {
    fn for_job(state: PipelineState) -> Self {
        match state {
            PipelineState::InProgress | PipelineState::Canceled | PipelineState::Manual => {
                Self::Incomplete
            },
            PipelineState::Failed => Self::Failing,
            PipelineState::Success => Self::Passing,
        }
    }

    fn combine(self, other: Self) -> Self {
        match (self, other) {
            (x, Self::Missing) | (Self::Missing, x) => x,
            (_, Self::Failing) | (Self::Failing, _) => Self::Failing,
            (_, Self::Incomplete) | (Self::Incomplete, _) => Self::Incomplete,
            (Self::Passing, Self::Passing) => Self::Passing,
        }
    }
}

impl merge::MergePolicy for WorkflowMergePolicy {
    type Filter = WorkflowMergePolicyFilter;

    fn for_mr(&self, mr: &MergeRequest) -> Self::Filter {
        let mut trailers = vec![Trailer::new("Acked-by", format!("{}", self.identity))];
        let mut ci_policy_errors = Vec::new();

        if let Some(ci_policy) = self.ci_policy.as_ref() {
            info!(
                target: "ghostflow-director/merge_policy",
                "applying merge policy to {}",
                mr.url,
            );

            let stage_states = {
                let pipelines = match ci_policy.service.pipelines_for_mr(mr) {
                    Ok(pipelines) => pipelines.unwrap_or_default(),
                    Err(err) => {
                        warn!(
                            target: "ghostflow-director/merge_policy",
                            "failed to get pipelines for {}: {}",
                            mr.url, err,
                        );
                        Vec::new()
                    },
                };
                let jobs = pipelines.iter().flat_map(|pipeline| {
                    match ci_policy.service.pipeline_jobs(pipeline) {
                        Ok(jobs) => jobs.unwrap_or_default(),
                        Err(err) => {
                            warn!(
                                target: "ghostflow-director/merge_policy",
                                "failed to get jobs for pipeline {} in {}: {}",
                                pipeline.id, mr.url, err,
                            );
                            Vec::new()
                        },
                    }
                });

                info!(
                    target: "ghostflow-director/merge_policy",
                    "found {} pipelines for {}",
                    pipelines.len(), mr.url,
                );

                let mut stage_states: BTreeMap<String, StageState> = BTreeMap::new();

                for job in jobs {
                    let stage = job.stage.unwrap_or_default();
                    let job_state = StageState::for_job(job.state);

                    info!(
                        target: "ghostflow-director/merge_policy",
                        "found job {} with state {:?} in stage {}",
                        job.name, job.state, stage,
                    );

                    stage_states
                        .entry(stage)
                        .and_modify(|ss| *ss = ss.combine(job_state))
                        .or_insert(job_state);
                }

                stage_states
            };

            for stage_policy in &ci_policy.conf.stage_policies {
                let stage_state = if stage_policy.stages.is_empty() {
                    stage_states
                        .values()
                        .fold(StageState::default(), |ss, ss_new| ss.combine(*ss_new))
                } else {
                    stage_policy
                        .stages
                        .iter()
                        .fold(StageState::default(), |ss, stage| {
                            let ss_new = stage_states.get(stage).copied().unwrap_or_default();
                            ss.combine(ss_new)
                        })
                };

                let review_action = match stage_state {
                    StageState::Missing => stage_policy.missing,
                    StageState::Failing => stage_policy.failing,
                    StageState::Incomplete => stage_policy.incomplete,
                    StageState::Passing => stage_policy.passing,
                };

                info!(
                    target: "ghostflow-director/merge_policy",
                    "stage state {:?} results in review action {:?}",
                    stage_state, review_action,
                );

                if let Some(action) = review_action {
                    // Add the rejection reason if needed.
                    if action == io::CiReviewAction::Reject {
                        if let Some(reason) = stage_policy.reject_reason.as_ref() {
                            info!(
                                target: "ghostflow-director/merge_policy",
                                "rejecting merge due to {} for {}",
                                reason, mr.url,
                            );

                            ci_policy_errors.push(reason.clone());
                        }
                    }

                    // Add the trailer specified by the CI policy.
                    if let Some(trailer) = action.trailer() {
                        info!(
                            target: "ghostflow-director/merge_policy",
                            "adding trailer {} for {}",
                            trailer, mr.url,
                        );

                        trailers.push(Trailer::new(trailer, format!("{}", ci_policy.identity)));
                    }

                    break;
                }
            }
        }

        let bypass_token = self.bypass_secret.as_ref().map(|bypass_secret| {
            let mut hasher = Sha256::new();

            // Add a salt from our side.
            hasher.update(GHOSTFLOW_BYPASS_SALT);
            // Add in the URL of the merge request.
            hasher.update(mr.url.as_bytes());
            // Add in the last commit of the merge request (so that old bypass tokens cannot be
            // provided for subsequent updates).
            let commit = format!("{}", mr.commit.id);
            hasher.update(commit.as_bytes());
            // Add in the policy-specific secret.
            hasher.update(bypass_secret.as_bytes());

            format!("{}{:x}", GHOSTFLOW_BYPASS_PREFIX, hasher.finalize())
        });

        WorkflowMergePolicyFilter {
            trailers,
            ci_policy_errors,
            errors: Vec::new(),
            bypass_info: CiBypassInformation {
                expected: bypass_token,
                actual: None,
                errors: Vec::new(),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use ghostflow::host::PipelineState;
    use git_workarea::GitContext;
    use itertools::Itertools;
    use serde_json::{json, Value};

    use super::StageState;
    use crate::handlers::test::*;

    #[test]
    fn test_stage_state_default() {
        assert_eq!(StageState::default(), StageState::Missing);
    }

    #[test]
    fn test_stage_state_combinations() {
        let combinations = [
            (
                StageState::Missing,
                StageState::Missing,
                StageState::Missing,
            ),
            (
                StageState::Missing,
                StageState::Failing,
                StageState::Failing,
            ),
            (
                StageState::Missing,
                StageState::Incomplete,
                StageState::Incomplete,
            ),
            (
                StageState::Missing,
                StageState::Passing,
                StageState::Passing,
            ),
            (
                StageState::Failing,
                StageState::Missing,
                StageState::Failing,
            ),
            (
                StageState::Failing,
                StageState::Failing,
                StageState::Failing,
            ),
            (
                StageState::Failing,
                StageState::Incomplete,
                StageState::Failing,
            ),
            (
                StageState::Failing,
                StageState::Passing,
                StageState::Failing,
            ),
            (
                StageState::Incomplete,
                StageState::Missing,
                StageState::Incomplete,
            ),
            (
                StageState::Incomplete,
                StageState::Failing,
                StageState::Failing,
            ),
            (
                StageState::Incomplete,
                StageState::Incomplete,
                StageState::Incomplete,
            ),
            (
                StageState::Incomplete,
                StageState::Passing,
                StageState::Incomplete,
            ),
            (
                StageState::Passing,
                StageState::Missing,
                StageState::Passing,
            ),
            (
                StageState::Passing,
                StageState::Failing,
                StageState::Failing,
            ),
            (
                StageState::Passing,
                StageState::Incomplete,
                StageState::Incomplete,
            ),
            (
                StageState::Passing,
                StageState::Passing,
                StageState::Passing,
            ),
        ];

        for (a, b, expect) in &combinations {
            assert_eq!(a.combine(*b), *expect);
        }
    }

    #[test]
    fn test_stage_state_job_state() {
        let states = [
            (PipelineState::InProgress, StageState::Incomplete),
            (PipelineState::Canceled, StageState::Incomplete),
            (PipelineState::Manual, StageState::Incomplete),
            (PipelineState::Failed, StageState::Failing),
            (PipelineState::Success, StageState::Passing),
        ];

        for (p, s) in &states {
            assert_eq!(StageState::for_job(*p), *s);
        }
    }

    fn check_commit_message(ctx: &GitContext, branch: &str, expected: &str) {
        let cat_file = ctx
            .git()
            .arg("cat-file")
            .arg("-p")
            .arg(format!("refs/heads/{}", branch))
            .output()
            .unwrap();
        assert!(cat_file.status.success());
        let actual = String::from_utf8_lossy(&cat_file.stdout)
            .splitn(2, "\n\n")
            .skip(1)
            .join("\n\n");
        assert_eq!(actual, expected);
    }

    fn check_merge_commit(ctx: &GitContext, branch: &str, topic: &str, expected: &str) {
        check_commit_message(ctx, branch, expected);

        let log = ctx
            .git()
            .arg("log")
            .arg("--pretty=%an%n%ae%n%cn%n%ce%n%P")
            .arg(format!("refs/heads/{}", branch))
            .output()
            .unwrap();
        assert!(log.status.success());
        let lines = String::from_utf8_lossy(&log.stdout);
        let lines = lines.lines().collect::<Vec<_>>();

        let author_name = lines[0];
        assert_eq!(author_name, "Ghostflow Director");

        let author_email = lines[1];
        assert_eq!(author_email, "ghostflow-director-testing@example.com");

        let committer_name = lines[2];
        assert_eq!(committer_name, "Ghostflow Director");

        let committer_email = lines[3];
        assert_eq!(committer_email, "ghostflow-director@example.com");

        let parents = lines[4].split_whitespace().collect::<Vec<_>>();
        assert_eq!(parents.len(), 2);
        assert_eq!(parents[1], topic);
    }

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    fn test_merge_config() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "merge": {
                            "required_access_level": "maintainer",
                        },
                    },
                },
            },
        })
    }

    fn test_merge_policy_config() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "merge": {
                            "required_access_level": "maintainer",
                            "policy": {
                                "ci_pipeline": {
                                    "ci_user": "ci-tester",
                                    "stage_policies": [
                                        {
                                            "stages": ["passing"],
                                            "passing": "tested",
                                        },
                                        {
                                            "stages": ["passing-ack"],
                                            "passing": "ack",
                                        },
                                        {
                                            "stages": ["passing-review"],
                                            "passing": "review",
                                        },
                                        {
                                            "stages": ["incomplete"],
                                            "incomplete": "ack",
                                        },
                                        {
                                            "stages": ["failing"],
                                            "failing": "reject",
                                            "reject_reason": "CI is failing; please check testing.",
                                        },
                                        {
                                            "stages": ["failing-silent"],
                                            "failing": "reject",
                                        },
                                        {
                                            "stages": ["ignore"],
                                            "passing": "ignore",
                                        },
                                        {
                                            "stages": ["missing"],
                                            "missing": "ack",
                                        },
                                        {
                                            "stages": ["missing-catch"],
                                            "missing": "review",
                                        },
                                    ],
                                },
                            },
                        },
                    },
                },
            },
        })
    }

    fn test_merge_policy_config_bypass() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "merge": {
                            "required_access_level": "maintainer",
                            "policy": {
                                "bypass_secret": "s00per-secret",
                                "ci_pipeline": {
                                    "ci_user": "ci-tester",
                                    "stage_policies": [
                                        {
                                            "stages": ["passing"],
                                            "passing": "tested",
                                        },
                                        {
                                            "stages": ["passing-ack"],
                                            "passing": "ack",
                                        },
                                        {
                                            "stages": ["passing-review"],
                                            "passing": "review",
                                        },
                                        {
                                            "stages": ["incomplete"],
                                            "incomplete": "ack",
                                        },
                                        {
                                            "stages": ["failing"],
                                            "failing": "reject",
                                            "reject_reason": "CI is failing; please check testing.",
                                        },
                                        {
                                            "stages": ["failing-silent"],
                                            "failing": "reject",
                                        },
                                        {
                                            "stages": ["ignore"],
                                            "passing": "ignore",
                                        },
                                        {
                                            "stages": ["missing"],
                                            "missing": "ack",
                                        },
                                        {
                                            "stages": ["missing-catch"],
                                            "missing": "review",
                                        },
                                    ],
                                },
                            },
                        },
                    },
                },
            },
        })
    }

    fn test_merge_policy_config_multi_stages() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "merge": {
                            "required_access_level": "maintainer",
                            "policy": {
                                "ci_pipeline": {
                                    "ci_user": "ci-tester",
                                    "stage_policies": [
                                        {
                                            "stages": ["passing", "failing"],
                                            "passing": "tested",
                                            "failing": "reject",
                                        },
                                        {
                                            "passing": "tested",
                                            "failing": "reject",
                                            "reject_reason": "CI is failing; please check testing.",
                                        },
                                    ],
                                },
                            },
                        },
                    },
                },
            },
        })
    }

    #[test]
    fn test_merge_policy_ignore_unknown_trailers() {
        let mut service = TestService::new(
            "test_merge_policy_ignore_unknown_trailers",
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Random-trailer: hello"),
                Action::mr_comment("ghostflow", "Reported-by: me"),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Reported-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }
    #[test]
    fn test_merge_policy_ci_missing_behavior() {
        let mut service = TestService::new(
            "test_merge_policy_ci_missing_behavior",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Acked-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_failing_behavior() {
        let mut service = TestService::new(
            "test_merge_policy_ci_failing_behavior",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Failed, "failing-silent", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Rejected-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_incomplete_behavior() {
        let mut service = TestService::new(
            "test_merge_policy_ci_incomplete_behavior",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::InProgress, "incomplete", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Acked-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_passing_behavior() {
        let mut service = TestService::new(
            "test_merge_policy_ci_passing_behavior",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Tested-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_ack_reaction() {
        let mut service = TestService::new(
            "test_merge_policy_ci_ack_reaction",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing-ack", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Acked-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_review_reaction() {
        let mut service = TestService::new(
            "test_merge_policy_ci_review_reaction",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing-review", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Reviewed-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_tested_reaction() {
        let mut service = TestService::new(
            "test_merge_policy_ci_tested_reaction",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Tested-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_reject_reaction() {
        let mut service = TestService::new(
            "test_merge_policy_ci_reject_reaction",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Failed, "failing-silent", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Rejected-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_reject_reason_reaction() {
        let mut service = TestService::new(
            "test_merge_policy_ci_reject_reason_reaction",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Failed, "failing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "This merge request may not be merged into `master` because:\n\n  \
                 - CI is failing; please check testing."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_merge_policy_ci_ignore_reaction() {
        let mut service = TestService::new(
            "test_merge_policy_ci_ignore_reaction",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "ignore", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_multiple_stages() {
        let mut service = TestService::new(
            "test_merge_policy_ci_multiple_stages",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing", "job"),
                Action::create_job(PipelineState::Failed, "failing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config_multi_stages();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Rejected-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_ci_all_stages() {
        let mut service = TestService::new(
            "test_merge_policy_ci_all_stages",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing", "job"),
                Action::create_job(PipelineState::InProgress, "failing", "job"),
                Action::create_job(PipelineState::Failed, None, "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config_multi_stages();
        let end = EndSignal::MergeRequestComment {
            content: "This merge request may not be merged into `master` because:\n\n  \
                 - CI is failing; please check testing."
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_merge_policy_bypass_unexpected() {
        let mut service = TestService::new(
            "test_merge_policy_bypass_unexpected",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment(
                    "ghostflow",
                    "Bypass-token: secret-token:gdbt-deadbeef\nDo: merge",
                ),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "This merge request may not be merged into `master` because:\n\n  \
                 - Invalid bypass token provided"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_merge_policy_bypass_multiple() {
        let mut service = TestService::new(
            "test_merge_policy_bypass_multiple",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Bypass-token: gdbt-deadbeef"),
                Action::mr_comment("ghostflow", "Bypass-token: gdbt-cafed00d"),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config();
        let end = EndSignal::MergeRequestComment {
            content: "This merge request may not be merged into `master` because:\n\n  \
                 - Multiple bypass tokens provided; at most one is permitted  \n  \
                 - Invalid bypass token provided"
                .into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_merge_policy_bypass_success() {
        let mut service = TestService::new(
            "test_merge_policy_bypass_success",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Failed, "failing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Bypass-token: gdbt-76ec583d325b39804258dd90ec676102872efdddaaaa25824991069f5c609d9a"),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config_bypass();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Rejected-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }

    #[test]
    fn test_merge_policy_bypass_unnecessary() {
        let mut service = TestService::new(
            "test_merge_policy_bypass_success",
            [
                Action::create_user("fork"),
                Action::create_user("ci-tester"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::create_pipeline("ghostflow", "example", MERGE_REQUEST_TOPIC),
                Action::create_job(PipelineState::Success, "passing", "job"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", "Do: merge"),
            ],
        )
        .unwrap();
        let config = test_merge_policy_config_bypass();
        let end = EndSignal::MergeRequestComment {
            content: "Topic successfully merged and pushed.".into(),
        };

        service.launch(config, end).unwrap();

        let project_dir = service.root().join("projects/ghostflow/example.git");
        let ctx = GitContext::new(project_dir);

        check_merge_commit(
            &ctx,
            "master",
            MERGE_REQUEST_TOPIC,
            "Merge topic 'mr-source'\n\
             \n\
             6f781d4 fork-master: add a file\n\
             \n\
             Acked-by: Ghostflow Director <ghostflow-director-testing@example.com>\n\
             Tested-by: ci-tester <ci-tester@example.org>\n\
             Merge-request: !0\n",
        );
    }
}
