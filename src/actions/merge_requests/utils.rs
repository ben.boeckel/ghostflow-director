// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::iter;

use clap::{ColorChoice, Command};
use ghostflow::actions::check::{CheckError, CheckStatus};
use itertools::Itertools;
use log::error;

use crate::actions::merge_requests::{Backport, Info};
use crate::config::{Branch, Project};

pub fn command_app(name: &'static str) -> Command {
    Command::new(name)
        .color(ColorChoice::Never)
        .disable_version_flag(true)
        .no_binary_name(true)
}

pub enum CheckResult {
    Pass,
    Fail,
}

/// Check a merge request against the target branch's checks.
pub fn check_mr(project: &Project, branch: &Branch, mr: &Info) -> Result<CheckResult, CheckError> {
    let mut errors = Vec::new();

    let result = mr
        .backports()
        .into_iter()
        .chain(iter::once(Backport::main_target(&branch.name)))
        .map(|backport| {
            let branch = if let Some(branch) = project.branches.get(backport.branch()) {
                branch
            } else {
                errors.push(format!(
                    "the `{}` target branch is not watched",
                    backport.branch(),
                ));
                return Ok(CheckStatus::Fail);
            };
            let commit = match backport.commit(&mr.merge_request, &project.context) {
                Ok(commit) => commit,
                Err(err) => {
                    error!(
                        target: "ghostflow-director/handler",
                        "failed to determine the commit to backport into {}: {:?}",
                        backport.branch(),
                        err,
                    );

                    errors.push(format!(
                        "failed to determine the commit to backport into {}: {}",
                        backport.branch(),
                        err,
                    ));
                    return Ok(CheckStatus::Fail);
                },
            };
            project.check_mr_with(branch, &mr.merge_request, &commit)
        })
        // Collect all of the results.
        .collect::<Result<Vec<_>, CheckError>>()
        // Ensure that all of them passed.
        .map(|results| {
            if results
                .into_iter()
                .all(|status| status == CheckStatus::Pass)
            {
                CheckResult::Pass
            } else {
                CheckResult::Fail
            }
        });

    if !errors.is_empty() {
        let comment = format!(
            "The backport configuration is not valid:\n\
             \n  - {}",
            errors.into_iter().join("\n  - "),
        );
        if let Err(err) = project.service.post_mr_comment(&mr.merge_request, &comment) {
            error!(
                target: "ghostflow-director/handler",
                "failed to post backport configuration failure comment: {:?}",
                err,
            );
        }
    }

    result
}
