// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use clap::{Arg, ArgAction};

use crate::actions::merge_requests::utils;
use crate::actions::merge_requests::{Action, ActionError, Data, Effect};

/// A command which extracts help information from a set of named commands.
///
/// Given an array containing pairs of command names and action implementations, this command will
/// generate content for commands which are supported (as they define themselves through their
/// `Action::help` implementation).
#[derive(Clone, Copy)]
pub struct Help<'a> {
    commands: &'a [(&'a str, &'a dyn Action)],
}

impl<'a> Help<'a> {
    /// Create a new `Help` action for a set of named actions.
    pub fn new(commands: &'a [(&'a str, &'a dyn Action)]) -> Self {
        Self {
            commands,
        }
    }
}

impl<'a> Action for Help<'a> {
    fn help(&self, _: &Data) -> Option<Cow<'static, str>> {
        None
    }

    fn perform(&self, arguments: &[&str], data: &Data) -> Vec<Effect> {
        let mut effects = Vec::new();

        let matches = utils::command_app("check")
            // Ignored (allowed for compatibility).
            .arg(
                Arg::new("ALL")
                    .short('a')
                    .long("all")
                    .action(ArgAction::SetTrue),
            )
            .try_get_matches_from(arguments);
        let matches = match matches {
            Ok(matches) => matches,
            Err(err) => {
                effects.push(ActionError::unrecognized_arguments(err).into());
                return effects;
            },
        };

        if matches.get_flag("ALL") {
            let msg =
                "Note that the '--all' flag is deprecated; all commands are shown unconditionally.";
            effects.push(Effect::message(msg));
        }

        self.commands
            .iter()
            .filter_map(|(command, action)| action.help(data).map(|help| (command, help)))
            .for_each(|(command, help_text)| {
                let msg = format!("The `{}` action: {}", command, help_text);
                effects.push(Effect::message(msg));
            });

        effects
    }
}

#[cfg(test)]
mod tests {
    use std::fs;

    use serde_json::{json, Value};

    use crate::handlers::test::*;

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    fn test_basic_config(inner: Value) -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": inner,
                },
            },
        })
    }

    fn test_command<C, F>(test_name: &str, command: &str, comment: C, config: F)
    where
        C: Into<String>,
        F: Fn(&TestService) -> Value,
    {
        let mut service = TestService::new(
            test_name,
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::EnablePipelines,
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", command),
            ],
        )
        .unwrap();
        let config = test_basic_config(config(&service));
        let end = EndSignal::MergeRequestComment {
            content: comment.into(),
        };

        service.launch(config, end).unwrap();
    }

    #[test]
    fn test_help_command_all() {
        let config = |_: &TestService| json!({});

        let expect = "\
             Messages:\n\n  \
             - Note that the '--all' flag is deprecated; all commands are shown unconditionally.\n  \
             - The `check` action: Performs checks on the content and structure of the merge \
               request according to the project's settings. Generally, all messages should be \
               addressed. Those which mention a specific commit require that the commit itself be \
               fixed. That is, an additional commit which fixes the issue will not address the \
               problem with the original commit.\
            ";
        test_command("test_help_command_all", "Do: help --all", expect, config);
    }

    #[test]
    fn test_help_command_empty() {
        let config = |_: &TestService| json!({});

        let expect = "\
             Messages:\n\n  \
             - The `check` action: Performs checks on the content and structure of the merge \
               request according to the project's settings. Generally, all messages should be \
               addressed. Those which mention a specific commit require that the commit itself be \
               fixed. That is, an additional commit which fixes the issue will not address the \
               problem with the original commit.\
            ";
        test_command("test_help_command_empty", "Do: help", expect, config);
    }

    #[test]
    fn test_help_command_full() {
        let config = |service: &TestService| {
            let test_job_dir = service.root().join("test-jobs");
            fs::create_dir_all(&test_job_dir).unwrap();
            json!({
                "merge": {
                    "required_access_level": "maintainer",
                },
                "reformat": {
                    "required_access_level": "developer",
                    "formatters": [],
                },
                "stage": {
                    "required_access_level": "developer",
                },
                "tests": {
                    "required_access_level": "developer",
                    "backends": {
                        "jobs": {
                            "backend": "jobs",
                            "config": {
                                "queue": test_job_dir,
                                "help_string": "the jobs backend.",
                            },
                        },
                        "pipelines": {
                            "backend": "pipelines",
                        },
                        "refs": {
                            "backend": "refs",
                        },
                    },
                },
            })
        };

        let expect = "\
             Messages:\n\n  \
             - The `check` action: Performs checks on the content and structure of the merge \
               request according to the project's settings. Generally, all messages should be \
               addressed. Those which mention a specific commit require that the commit itself be \
               fixed. That is, an additional commit which fixes the issue will not address the \
               problem with the original commit.\n  \
             - The `merge` action: Merges the merge request topic into the target branch (or \
               branches in the case of backports). It fails if the merge request has not passed \
               the checks or it is considered a work-in-progress. The name of the topic merged \
               into the target branch may be changed with either the `--topic` argument or using \
               a `Topic-rename` trailer in the description.\n  \
             - The `reformat` action: Rewrites the topic for the merge request according to the \
               project coding guidelines. By default, every commit will have its content changed \
               so that it follows the coding guidelines. With the `--whole-tree` option, all \
               files in the repository as of the last commit in the topic will be rewritten to \
               follow the coding guidelines. Any commit which is empty after the reformatting \
               will be dropped from the topic. After performing a reformat, the resulting topic \
               is force-pushed as the source topic of the merge request. In order to use the \
               reformatted branch, it must be fetched and updated locally, otherwise the \
               reformatting will need to be redone.\n  \
             - The `stage` action: Takes the topic from the merge request and adds it to the \
               \"stage\". This is a branch which contains all of the staged topics merged in on \
               top of the target branch. Conflicts between topics on the stage are handled in a \
               \"first-come, first-served\" fashion, so if a topic conflicts with another, \
               waiting until it is either merged into the target branch or removed from the stage \
               is best. When updating a merge request with new code, a topic that has already \
               been staged is removed from the stage.\n  \
             - The `test` action: Multiple backends are supported. In order to restrict \
               communication to a given backend, the first argument may be in the form of \
               `-Xbackend` (without a space).\n\n  \
               * Backend `jobs`: the jobs backend.  \n  \
               * Backend `pipelines`: Triggers the service's CI APIs. It can start or restart CI \
                 jobs within the merge request's latest pipeline. It supports the `--action` (or \
                 `-a`) flag to select the action to perform. Supported actions are:\n\n  \
                 - `manual`: Start jobs awaiting manual action\n  \
                 - `unsuccessful`: Restart jobs which completed without success (including \
                   skipped or canceled jobs)\n  \
                 - `failed`: Restart jobs which completed with failure\n  \
                 - `completed`: Restart all completed jobs\n\n\
                 Jobs affected by the action may also be filtered. The `--stage` (or `-s`) flag \
                 may be used to give the name of a stage to perform the action on. The `--named` \
                 (or `-n`) flag may be used multiple times to only affect jobs which match the \
                 argument. When given multiple `--named` arguments, a job may match any of the \
                 arguments to be affected.  \n  \
               * Backend `refs`: Creates a special reference on the repository which indicates \
                 that the merge request should be tested. Test machines look at these special \
                 references and run the tests. The `--stop` argument may be given to remove the \
                 special reference. The reference is also removed when the merge request is closed \
                 (for any reason). Updates to the merge request leave any existing test reference \
                 in place and require either a new `Do: test` to update it to the new code or a \
                 `Do: test --stop` to remove it.  \n  \
             - The `unstage` action: Removes the merge request topic from the stage.\
            ";
        test_command("test_help_command(Do)", "Do: help", expect, config);
        test_command("test_help_command(do)", "do: help", expect, config);
    }
}
