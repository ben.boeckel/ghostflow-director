// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// XXX(rust-1.66)]
#![allow(clippy::uninlined_format_args)]

//! The Kitware robot
//!
//! This program watches a directory for JSON jobs in order to manage the Kitware workflow for
//! repositories.

#![warn(missing_docs)]

use std::error::Error;
use std::fmt;
use std::io::{self, Write};
use std::path::{Path, PathBuf};

use clap::builder::PossibleValuesParser;
use clap::{Arg, ArgAction, Command};
use json_job_dispatch::{self, Director, DirectorError, DirectorWatchdog, HandlerCore, RunResult};
use log::LevelFilter;
#[cfg(feature = "systemd")]
use log::SetLoggerError;
use rayon::ThreadPoolBuilder;
use serde_json::json;
#[cfg(feature = "systemd")]
use systemd::journal::JournalLog;
use thiserror::Error;

#[macro_use]
pub mod config;
use crate::config::{Config, ConfigRead};

mod ghostflow_ext;

pub mod actions;
pub mod handlers;

#[cfg(test)]
mod tests;

// Include git information from `build.rs`.
include!(concat!(env!("OUT_DIR"), "/git_info.rs"));

#[derive(Debug, Error)]
enum RunError {
    #[error("config error: {}", source)]
    Config {
        #[from]
        source: config::ConfigError,
    },
    #[error("failed to create director in {}: {}", path.display(), source)]
    CreateDirector {
        path: PathBuf,
        #[source]
        source: json_job_dispatch::DirectorError,
    },
    #[error("failed to add watchdog to the director: {}", source)]
    AddWatchdog {
        #[source]
        source: Box<dyn Error + Send + Sync>,
    },
    #[error("failed to add handler to the director: {}", source)]
    AddHandler {
        #[source]
        source: Box<dyn Error + Send + Sync>,
    },
    #[error("failed to run the director in {}: {}", path.display(), source)]
    RunDirector {
        path: PathBuf,
        #[source]
        source: DirectorError,
    },
}

impl RunError {
    fn create_director(path: PathBuf, source: json_job_dispatch::DirectorError) -> Self {
        RunError::CreateDirector {
            path,
            source,
        }
    }

    fn add_watchdog(source: Box<dyn Error + Send + Sync>) -> Self {
        RunError::AddWatchdog {
            source,
        }
    }

    fn add_handler(source: Box<dyn Error + Send + Sync>) -> Self {
        RunError::AddHandler {
            source,
        }
    }

    fn run_director(path: PathBuf, source: DirectorError) -> Self {
        RunError::RunDirector {
            path,
            source,
        }
    }
}

/// Run the director with the configuration file's path.
fn run_director(config_path: &Path) -> Result<RunResult, RunError> {
    let watchdog = DirectorWatchdog {};

    let config = Config::from_path(config_path)?;

    let mut director = Director::new(&config.archive_dir)
        .map_err(|err| RunError::create_director(config.archive_dir.clone(), err))?;

    watchdog
        .add_to_director(&mut director)
        .map_err(RunError::add_watchdog)?;
    for handler in &config.handlers {
        handler
            .add_to_director(&mut director)
            .map_err(RunError::add_handler)?;
    }

    director
        .watch_directory(&config.queue_dir)
        .map_err(|err| RunError::run_director(config.queue_dir, err))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Job {
    Exit,
    Restart,
    ResetFailedProjects,
}

impl fmt::Display for Job {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let desc = match self {
            Job::Exit => "exit",
            Job::Restart => "restart",
            Job::ResetFailedProjects => "reset failed projects",
        };

        write!(f, "{}", desc)
    }
}

#[derive(Debug, Error)]
enum SetupError {
    #[cfg(not(feature = "systemd"))]
    #[error("unavailable logger: {}", name)]
    UnavailableLogger { name: &'static str },
    #[cfg(feature = "systemd")]
    #[error("failed to initialize the journal: {}", source)]
    InitializeJournal {
        #[from]
        source: SetLoggerError,
    },
    #[error("unrecognized logger: {}", name)]
    UnrecognizedLogger { name: String },
    #[error("failed to initialize the global rayon thread pool: {}", source)]
    RayonThreadPoolInit {
        #[from]
        source: rayon::ThreadPoolBuildError,
    },
    #[error("failed to handle the {} job: {}", job, source)]
    JobError {
        job: Job,
        #[source]
        source: json_job_dispatch::utils::JobError,
    },
    #[error("failed to archive: {}", source)]
    Archive {
        #[from]
        source: json_job_dispatch::utils::ArchiveQueueError,
    },
}

impl SetupError {
    #[cfg(not(feature = "systemd"))]
    fn unavailable_logger(name: &'static str) -> Self {
        SetupError::UnavailableLogger {
            name,
        }
    }

    fn unrecognized_logger(name: String) -> Self {
        SetupError::UnrecognizedLogger {
            name,
        }
    }

    fn job_error(job: Job, source: json_job_dispatch::utils::JobError) -> Self {
        SetupError::JobError {
            job,
            source,
        }
    }
}

enum Logger {
    #[cfg(feature = "systemd")]
    Systemd,
    Env,
}

/// A `main` function which supports `try!`.
fn try_main() -> Result<(), Box<dyn Error>> {
    let matches = Command::new("ghostflow-director")
        .version(VERSION_STRING)
        .author("Ben Boeckel <ben.boeckel@kitware.com>")
        .about("Watch a directory for project events and perform workflow actions on them")
        .arg(
            Arg::new("CONFIG")
                .short('c')
                .long("config")
                .help("Path to the configuration file")
                .required(true)
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("DEBUG")
                .short('d')
                .long("debug")
                .help("Increase verbosity")
                .action(ArgAction::Count),
        )
        .arg(
            Arg::new("LOGGER")
                .short('l')
                .long("logger")
                .default_value("env")
                .value_parser(PossibleValuesParser::new([
                    "env",
                    #[cfg(feature = "systemd")]
                    "systemd",
                ]))
                .help("Logging backend")
                .value_name("LOGGER")
                .action(ArgAction::Set),
        )
        .arg(
            Arg::new("VERIFY")
                .short('v')
                .long("verify")
                .help("Check the configuration file and exit")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("DUMP_CONFIG")
                .long("dump-config")
                .help("Read the configuration file, dump it as JSON, and exit")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("RESET_FAILED_PROJECTS")
                .long("reset-failed-projects")
                .help("Reset failed projects so that they are attempted again")
                .value_name("HOST")
                .action(ArgAction::Set)
                .number_of_values(1)
                .action(ArgAction::Append),
        )
        .arg(
            Arg::new("THREAD_COUNT")
                .short('j')
                .long("threads")
                .help("Number of threads to use in the Rayon thread pool")
                .action(ArgAction::Set)
                .value_parser(clap::value_parser!(usize)),
        )
        .arg(
            Arg::new("RELOAD")
                .long("reload")
                .help("Reload the configuration")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("EXIT")
                .long("exit")
                .help("Cause the director to exit")
                .action(ArgAction::SetTrue),
        )
        .arg(
            Arg::new("ARCHIVE")
                .long("archive")
                .help("Archive jobs into tarballs in an archive directory")
                .action(ArgAction::Set),
        )
        .get_matches();

    let log_level = match matches.get_one::<u8>("DEBUG").copied().unwrap_or(0) {
        0 => LevelFilter::Error,
        1 => LevelFilter::Warn,
        2 => LevelFilter::Info,
        3 => LevelFilter::Debug,
        _ => LevelFilter::Trace,
    };

    let _logger = match matches
        .get_one::<String>("LOGGER")
        .expect("logger should have a value")
        .as_ref()
    {
        "env" => {
            env_logger::Builder::new().filter(None, log_level).init();
            Logger::Env
        },

        #[cfg(feature = "systemd")]
        "systemd" => {
            JournalLog::init()?;
            Logger::Systemd
        },
        #[cfg(not(feature = "systemd"))]
        "systemd" => {
            return Err(SetupError::unavailable_logger("systemd").into());
        },

        logger => {
            return Err(SetupError::unrecognized_logger(logger.into()).into());
        },
    };

    log::set_max_level(log_level);

    if let Some(count) = matches.get_one::<usize>("THREAD_COUNT").copied() {
        ThreadPoolBuilder::new().num_threads(count).build_global()?;
    }

    let config_path = Path::new(
        matches
            .get_one::<String>("CONFIG")
            .expect("the configuration option is required"),
    );

    if matches.get_flag("VERIFY") {
        let config = ConfigRead::from_path(config_path)?;
        config.verify_all_check_configs()?;
    } else if matches.get_flag("DUMP_CONFIG") {
        let config = ConfigRead::from_path(config_path)?;
        let stdout = io::stdout();
        let mut out = stdout.lock();
        serde_json::to_writer_pretty(&mut out, &config)?;
        out.write_all(b"\n")?;
    } else if matches.get_flag("EXIT") {
        let config = ConfigRead::from_path(config_path)?;
        json_job_dispatch::utils::exit(config.queue_dir())
            .map_err(|err| SetupError::job_error(Job::Exit, err))?;
    } else if matches.get_flag("RELOAD") {
        let config = ConfigRead::from_path(config_path)?;
        json_job_dispatch::utils::restart(config.queue_dir())
            .map_err(|err| SetupError::job_error(Job::Restart, err))?;
    } else if let Some(hosts) = matches.get_many::<String>("RESET_FAILED_PROJECTS") {
        let config = ConfigRead::from_path(config_path)?;
        let queue_dir = config.queue_dir();
        hosts
            .map(|host| {
                let kind = format!("{}:reset_failed_projects", host);
                json_job_dispatch::utils::drop_job(queue_dir, kind, json!({}))
                    .map_err(|err| SetupError::job_error(Job::ResetFailedProjects, err))
            })
            .collect::<Result<Vec<_>, _>>()?;
    } else if matches.contains_id("ARCHIVE") {
        let config = ConfigRead::from_path(config_path)?;
        let archive_path = matches
            .get_one::<String>("ARCHIVE")
            .expect("the archive option requires a value");
        json_job_dispatch::utils::archive_queue(config.archive_dir(), archive_path)?;
    } else {
        while let RunResult::Restart = run_director(config_path)? {}
    }

    Ok(())
}

/// The entry point.
///
/// Wraps around `try_main` to panic on errors.
fn main() {
    let _ = {
        // Pin `git-checks` into the binary.
        git_checks::InvalidUtf8::default()
    };

    if let Err(err) = try_main() {
        panic!("{:?}", err);
    }
}
