// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use chrono::Utc;
use ghostflow::host::{Comment, Commit, HostingServiceError};
use ghostflow_gitlab::gitlab::webhooks::{
    MergeRequestAction, MergeRequestHookAttrs, NoteHook, PushHook,
};
use ghostflow_gitlab::gitlab::MergeRequestState;
use git_workarea::CommitId;
use thiserror::Error;

use crate::actions::merge_requests::Info;
use crate::ghostflow_ext::DirectorHostingService;
use crate::handlers::common::data::*;

#[derive(Debug, Error)]
pub enum MergeRequestInfoError {
    #[error("the source project has been deleted")]
    DeletedSourceProject {},
    #[error("merge request information is missing from the note")]
    MissingMergeRequest {},
    #[error("host error: {}", source)]
    Host {
        #[from]
        source: HostingServiceError,
    },
}

impl MergeRequestInfoError {
    fn deleted_source_project() -> Self {
        MergeRequestInfoError::DeletedSourceProject {}
    }

    fn missing_merge_request() -> Self {
        MergeRequestInfoError::MissingMergeRequest {}
    }
}

/// Information for a Gitlab merge request.
pub struct GitlabMergeRequestInfo;

impl GitlabMergeRequestInfo {
    /// Create a merge request information block from a webhook.
    pub fn from_web_hook<'a>(
        service: &dyn DirectorHostingService,
        hook: &'a MergeRequestHookAttrs,
    ) -> Result<Info<'a>, MergeRequestInfoError> {
        let source_name = hook
            .source
            .as_ref()
            .map(|source| &source.path_with_namespace)
            .ok_or_else(MergeRequestInfoError::deleted_source_project)?;
        let source_repo = service.repo(source_name)?;
        let is_open = match hook.state {
            MergeRequestState::Opened | MergeRequestState::Reopened => true,
            MergeRequestState::Closed | MergeRequestState::Merged | MergeRequestState::Locked => {
                false
            },
        };
        let date = hook.updated_at;
        let mut mr = service.merge_request(&hook.target.path_with_namespace, hook.iid.value())?;
        mr.old_commit = hook.oldrev.as_ref().map(|oldrev| {
            Commit {
                repo: source_repo,
                refname: Some(hook.source_branch.clone()),
                id: CommitId::new(oldrev.value()),
                // We don't generally care about the pipeline for the previous commit of the MR.
                last_pipeline: None,
            }
        });

        Ok(Info {
            merge_request: Cow::Owned(mr),
            was_opened: hook.action == Some(MergeRequestAction::Open)
                || hook.action == Some(MergeRequestAction::Reopen),
            was_merged: hook.action == Some(MergeRequestAction::Merge),
            is_open,
            date: *date.as_ref(),
        })
    }
}

/// Information for a Gitlab merge request comment.
pub struct GitlabMergeRequestNoteInfo;

impl GitlabMergeRequestNoteInfo {
    /// Create a merge request comment information block from a webhook.
    pub fn from_web_hook<'a>(
        service: &dyn DirectorHostingService,
        hook: &'a NoteHook,
    ) -> Result<MergeRequestNoteInfo<'a>, MergeRequestInfoError> {
        let merge_request = hook
            .merge_request
            .as_ref()
            .ok_or_else(MergeRequestInfoError::missing_merge_request)?;
        let note = Comment {
            id: format!("{}", hook.object_attributes.id.value()),
            is_system: hook.object_attributes.system,
            is_branch_update: false,
            created_at: *hook.object_attributes.created_at.as_ref(),
            author: service.user(
                &merge_request.target.path_with_namespace,
                &hook.user.username,
            )?,
            // Remove ZERO WIDTH SPACE from the command. This can occur when copy/pasting contents
            // from an email or rendering of a command into a comment box. Due to its invisibility,
            // it causes confusion when the resulting `not recognized at all` text appears with the
            // invisible character hidden in there.
            content: hook.object_attributes.note.replace('\u{200b}', ""),
        };

        GitlabMergeRequestInfo::from_web_hook(service, merge_request).map(|mr_info| {
            MergeRequestNoteInfo {
                merge_request: mr_info,
                note,
            }
        })
    }
}

/// Information for a Gitlab push.
pub struct GitlabPushInfo;

impl GitlabPushInfo {
    /// Create a push information block from a webhook.
    pub fn from_web_hook(
        service: &dyn DirectorHostingService,
        hook: PushHook,
    ) -> Result<PushInfo, HostingServiceError> {
        let date = if let Some(commit) = hook.commits.last() {
            commit.timestamp
        } else {
            Utc::now()
        };
        let commit_id = CommitId::new(hook.after.value());
        let last_pipeline = if commit_id.as_str() == "0000000000000000000000000000000000000000" {
            None
        } else {
            service
                .commit(&hook.project.path_with_namespace, &commit_id)?
                .last_pipeline
        };

        Ok(PushInfo {
            commit: Commit {
                repo: service.repo(&hook.project.path_with_namespace)?,
                refname: Some(hook.ref_),
                id: commit_id,
                last_pipeline,
            },
            author: service.user(&hook.project.path_with_namespace, &hook.user_username)?,
            date,
        })
    }
}
