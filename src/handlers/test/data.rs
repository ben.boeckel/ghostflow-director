// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::collections::hash_map::HashMap;
use std::fmt;
use std::fs::File;
use std::io;
use std::mem;
use std::path::{Path, PathBuf};

use chrono::{DateTime, Utc};
use ghostflow::host::{CommitStatusState, PipelineState};
use git_workarea::{CommitId, GitContext, GitError};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use thiserror::Error;

use crate::ghostflow_ext::AccessLevel;
use crate::handlers::test::hooks;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Entity {
    User,
    Project,
    MergeRequest,
    MergeRequestComment,
    Issue,
    Comment,
    Award,
    Pipeline,
    Job,
}

impl fmt::Display for Entity {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let desc = match self {
            Entity::User => "user",
            Entity::Project => "project",
            Entity::MergeRequest => "merge request",
            Entity::MergeRequestComment => "merge request comment",
            Entity::Issue => "issue",
            Entity::Comment => "comment",
            Entity::Award => "award",
            Entity::Pipeline => "pipeline",
            Entity::Job => "job",
        };

        write!(f, "{}", desc)
    }
}

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum DataError {
    #[error("missing {}: {}", entity, id)]
    NoEntity { entity: Entity, id: u64 },
    #[error("missing {} by name: {}", entity, name)]
    NoNamedEntity { entity: Entity, name: String },
    #[error("missing any {}", entity)]
    NoEntities { entity: Entity },
}

impl DataError {
    fn no_entity(entity: Entity, id: u64) -> Self {
        DataError::NoEntity {
            entity,
            id,
        }
    }

    fn no_named_entity(entity: Entity, name: String) -> Self {
        DataError::NoNamedEntity {
            entity,
            name,
        }
    }

    fn no_entities(entity: Entity) -> Self {
        DataError::NoEntities {
            entity,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Hook {
    Push,
    MergeRequest,
    Project,
    Membership,
}

impl fmt::Display for Hook {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let desc = match self {
            Hook::Push => "push",
            Hook::MergeRequest => "merge request",
            Hook::Project => "project",
            Hook::Membership => "membership",
        };

        write!(f, "{}", desc)
    }
}

#[derive(Debug, Error)]
pub enum HookError {
    #[error("failed to create a {} hook: {}", hook, source)]
    Create {
        hook: Hook,
        #[source]
        source: Box<HookError>,
    },
    #[error("failed to serialize a {} hook: {}", kind, source)]
    Serialize {
        kind: String,
        #[source]
        source: serde_json::Error,
    },
    #[error("failed to create job file {}: {}", path.display(), source)]
    CreateJob {
        path: PathBuf,
        #[source]
        source: io::Error,
    },
    #[error("failed to write job to {}: {}", path.display(), source)]
    Write {
        path: PathBuf,
        #[source]
        source: serde_json::Error,
    },
    #[error("data error: {}", source)]
    Data {
        #[from]
        source: DataError,
    },
    #[error("failed to check if an MR was merged: {}", output)]
    CheckIfMerged { output: String },
    #[error("git error: {}", source)]
    Git {
        #[from]
        source: GitError,
    },
}

impl HookError {
    fn create(hook: Hook, source: HookError) -> Self {
        HookError::Create {
            hook,
            source: source.into(),
        }
    }

    fn serialize(kind: String, source: serde_json::Error) -> Self {
        HookError::Serialize {
            kind,
            source,
        }
    }

    fn create_job(path: PathBuf, source: io::Error) -> Self {
        HookError::CreateJob {
            path,
            source,
        }
    }

    fn write(path: PathBuf, source: serde_json::Error) -> Self {
        HookError::Write {
            path,
            source,
        }
    }

    fn check_if_merged(output: &[u8]) -> Self {
        HookError::CheckIfMerged {
            output: String::from_utf8_lossy(output).into(),
        }
    }
}

/// An updated reference in a repository.
#[derive(Deserialize, Debug, Clone)]
pub struct RefUpdate {
    #[serde(rename = "ref")]
    pub ref_: String,
    pub old: String,
    pub new: String,
}

impl fmt::Display for RefUpdate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}..{}", self.ref_, self.old, self.new)
    }
}

#[derive(Debug, Clone)]
pub struct User {
    pub id: u64,
    pub name: String,
    pub handle: String,
    pub email: String,
}

#[derive(Debug, Clone)]
pub struct Project {
    pub id: u64,
    pub name: String,
    pub url: String,
    pub permissions: HashMap<u64, AccessLevel>,
    pub parent: Option<u64>,
    pub webhooks: Vec<String>,
    pub pipelines_enabled: bool,
}

#[derive(Debug)]
pub struct MergeRequest {
    pub id: u64,
    pub source_project: u64,
    pub source_branch: String,
    pub target_project: u64,
    pub target_branch: String,
    pub url: String,
    pub description: String,
    pub old_commit: Option<CommitId>,
    pub commit: CommitId,
    pub author: u64,
    pub reference: String,
    pub work_in_progress: bool,
    pub remove_source_branch: bool,

    state: hooks::MergeRequestState,
    comments: Vec<u64>,
    awards: Vec<u64>,
}

#[derive(Debug)]
pub struct Issue {
    pub id: u64,
    pub project: u64,
    pub url: String,
    pub labels: Vec<String>,
    pub reference: String,
}

#[derive(Debug)]
pub struct CommitStatus {
    pub state: CommitStatusState,
    pub author: u64,
    pub name: String,
    pub description: String,
    pub target_url: Option<String>,

    commit: CommitId,
    project: u64,
}

#[derive(Debug)]
pub struct Comment {
    pub id: u64,
    pub is_branch_update: bool,
    pub created_at: DateTime<Utc>,
    pub author: u64,
    pub content: String,

    awards: Vec<u64>,
}

#[derive(Debug)]
pub struct Award {
    id: u64,
    pub name: String,
    pub author: u64,
}

#[derive(Debug)]
pub struct Pipeline {
    pub id: u64,

    commit: CommitId,
    project: u64,
}

#[derive(Debug)]
pub struct Job {
    pub id: u64,
    pipeline: u64,

    pub state: PipelineState,
    pub name: String,
    pub stage: Option<String>,
}

#[derive(Debug)]
pub struct Data {
    webhook_dir: PathBuf,

    projects: Vec<Project>,
    users: Vec<User>,
    merge_requests: Vec<MergeRequest>,
    issues: Vec<Issue>,
    statuses: Vec<CommitStatus>,
    comments: Vec<Comment>,
    awards: Vec<Award>,
    pipelines: Vec<Pipeline>,
    jobs: Vec<Job>,
}

impl Data {
    pub fn new(webhook_dir: PathBuf) -> Self {
        Data {
            webhook_dir,

            projects: Vec::new(),
            users: Vec::new(),
            merge_requests: Vec::new(),
            issues: Vec::new(),
            statuses: Vec::new(),
            comments: Vec::new(),
            awards: Vec::new(),
            pipelines: Vec::new(),
            jobs: Vec::new(),
        }
    }

    pub fn add_user<H, N, E>(&mut self, handle: H, name: N, email: E) -> u64
    where
        H: Into<String>,
        N: Into<String>,
        E: Into<String>,
    {
        let new_id = self.users.len() as u64;

        self.users.push(User {
            id: new_id,
            handle: handle.into(),
            name: name.into(),
            email: email.into(),
        });

        new_id
    }

    pub fn add_project<N, D>(
        &mut self,
        name: N,
        dir: D,
        owner: u64,
        parent: Option<u64>,
    ) -> Result<u64, HookError>
    where
        N: Into<String>,
        D: AsRef<Path>,
    {
        let new_id = self.projects.len() as u64;

        let project = Project {
            id: new_id,
            name: name.into(),
            url: dir.as_ref().to_string_lossy().into_owned(),
            permissions: [(owner, AccessLevel::Owner)].iter().cloned().collect(),
            parent,
            webhooks: Vec::new(),
            pipelines_enabled: false,
        };
        self.projects.push(project);

        let project = self.projects.last().expect("no projects after adding one?");
        self.project_hook(project)
            .map_err(|err| HookError::create(Hook::Project, err))?;

        Ok(new_id)
    }

    pub fn add_to_project<U>(
        &mut self,
        project: u64,
        user: U,
        access: AccessLevel,
    ) -> Result<(), HookError>
    where
        U: AsRef<str>,
    {
        let user_id = self.user(user.as_ref())?.id;
        let old = self
            .project_by_id_mut(project)?
            .permissions
            .insert(user_id, access);

        let project = self.project_by_id(project)?;

        if let Some(old_access) = old {
            self.membership_hook(
                project,
                user_id,
                old_access,
                hooks::MembershipAction::Removed,
            )
            .map_err(|err| HookError::create(Hook::Membership, err))?;
        }

        self.membership_hook(project, user_id, access, hooks::MembershipAction::Added)
            .map_err(|err| HookError::create(Hook::Membership, err))?;

        Ok(())
    }

    pub fn add_hook<H>(&mut self, project: u64, hook: H) -> Result<(), DataError>
    where
        H: Into<String>,
    {
        self.project_by_id_mut(project)?.webhooks.push(hook.into());

        Ok(())
    }

    // It's test code. Not too worried about it.
    #[allow(clippy::too_many_arguments)]
    pub fn add_mr<S, T, D>(
        &mut self,
        source_id: u64,
        source_branch: S,
        target_id: u64,
        target_branch: T,
        description: D,
        commit: CommitId,
        author: u64,
        wip: bool,
        remove: bool,
    ) -> Result<u64, HookError>
    where
        S: Into<String>,
        T: Into<String>,
        D: Into<String>,
    {
        let new_id = self.merge_requests.len() as u64;

        let mr = MergeRequest {
            id: new_id,
            source_project: source_id,
            source_branch: source_branch.into(),
            target_project: target_id,
            target_branch: target_branch.into(),
            url: format!("{}/merge_requests/{}", target_id, new_id),
            description: description.into(),
            old_commit: None,
            commit,
            author,
            reference: format!("!{}", new_id),
            work_in_progress: wip,
            remove_source_branch: remove,
            state: hooks::MergeRequestState::Open,

            comments: Vec::new(),
            awards: Vec::new(),
        };
        self.merge_requests.push(mr);

        let mr = self
            .merge_requests
            .last()
            .expect("no merge requests after adding one?");
        self.mr_hook(mr, hooks::MergeRequestAction::Open)
            .map_err(|err| HookError::create(Hook::MergeRequest, err))?;

        Ok(new_id)
    }

    #[allow(clippy::too_many_arguments)]
    pub fn add_status<N, D, T>(
        &mut self,
        project: u64,
        commit: &CommitId,
        author: u64,
        name: N,
        description: D,
        target_url: Option<T>,
        state: CommitStatusState,
    ) where
        N: Into<String>,
        D: Into<String>,
        T: Into<String>,
    {
        self.statuses.push(CommitStatus {
            state,
            author,
            name: name.into(),
            description: description.into(),
            target_url: target_url.map(Into::into),

            commit: commit.clone(),
            project,
        })
    }

    pub fn add_mr_status<N, D, T>(
        &mut self,
        author: u64,
        name: N,
        description: D,
        target_url: Option<T>,
        state: CommitStatusState,
    ) -> Result<(), DataError>
    where
        N: Into<String>,
        D: Into<String>,
        T: Into<String>,
    {
        let (commit, project) = {
            let mr = self
                .merge_requests
                .last_mut()
                .ok_or_else(|| DataError::no_entities(Entity::MergeRequest))?;

            (mr.commit.clone(), mr.target_project)
        };

        self.add_status(
            project,
            &commit,
            author,
            name.into(),
            description.into(),
            target_url,
            state,
        );

        Ok(())
    }

    pub fn add_mr_comment(&mut self, mr: Option<u64>, comment: u64) -> Result<(), HookError> {
        self.mr_or_last_mut(mr)?.comments.push(comment);

        let mr = self.mr_or_last(mr)?;
        self.mr_hook(mr, hooks::MergeRequestAction::Comment)
            .map_err(|err| HookError::create(Hook::MergeRequest, err))?;

        Ok(())
    }

    pub fn add_mr_award(&mut self, mr: Option<u64>, award: u64) -> Result<(), DataError> {
        self.mr_or_last_mut(mr)?.awards.push(award);

        Ok(())
    }

    pub fn add_issue(&mut self, project: u64) -> u64 {
        let new_id = self.issues.len() as u64;

        self.issues.push(Issue {
            id: new_id,
            project,
            url: format!("{}/issues/{}", project, new_id),
            labels: Vec::new(),
            reference: format!("#{}", new_id),
        });

        // TODO: make a webhook (issue create)

        new_id
    }

    pub fn add_issue_labels<I>(&mut self, issue: u64, labels: I) -> Result<(), DataError>
    where
        I: IntoIterator<Item = String>,
    {
        self.issue_mut(issue)?.labels.extend(labels);

        Ok(())
    }

    pub fn add_comment<C>(&mut self, author: u64, content: C) -> u64
    where
        C: Into<String>,
    {
        let new_id = self.comments.len() as u64;

        self.comments.push(Comment {
            id: new_id,
            is_branch_update: false,
            created_at: Utc::now(),
            author,
            content: content.into(),

            awards: Vec::new(),
        });

        new_id
    }

    pub fn add_comment_award(&mut self, comment: Option<u64>, award: u64) -> Result<(), DataError> {
        self.comment_or_last_mut(comment)?.awards.push(award);

        Ok(())
    }

    pub fn add_award<N>(&mut self, author: u64, name: N) -> u64
    where
        N: Into<String>,
    {
        let new_id = self.awards.len() as u64;

        self.awards.push(Award {
            id: new_id,
            name: name.into(),
            author,
        });

        new_id
    }

    pub fn add_pipeline(&mut self, project: u64, commit: CommitId) -> u64 {
        let new_id = self.pipelines.len() as u64;

        self.pipelines.push(Pipeline {
            id: new_id,
            project,
            commit,
        });

        new_id
    }

    pub fn enable_pipelines(&mut self) -> Result<(), DataError> {
        self.project_or_last_mut(None)?.pipelines_enabled = true;
        Ok(())
    }

    pub fn add_job(
        &mut self,
        state: PipelineState,
        stage: Option<String>,
        name: String,
    ) -> Result<u64, DataError> {
        let new_id = self.jobs.len() as u64;
        let pipeline_id = self.pipeline_or_last_mut(None)?.id;

        self.jobs.push(Job {
            id: new_id,
            pipeline: pipeline_id,
            state,
            stage,
            name,
        });

        Ok(new_id)
    }

    pub fn user_by_id(&self, id: u64) -> Result<&User, DataError> {
        self.users
            .iter()
            .find(|user| user.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::User, id))
    }

    pub fn user<N>(&self, name: N) -> Result<&User, DataError>
    where
        N: AsRef<str>,
    {
        self.users
            .iter()
            .find(|user| user.handle == name.as_ref())
            .ok_or_else(|| DataError::no_named_entity(Entity::User, name.as_ref().into()))
    }

    pub fn project_by_id(&self, id: u64) -> Result<&Project, DataError> {
        self.projects
            .iter()
            .find(|project| project.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::Project, id))
    }

    pub fn project<N>(&self, name: N) -> Result<&Project, DataError>
    where
        N: AsRef<str>,
    {
        self.projects
            .iter()
            .find(|project| project.name == name.as_ref())
            .ok_or_else(|| DataError::no_named_entity(Entity::Project, name.as_ref().into()))
    }

    fn project_by_id_mut(&mut self, id: u64) -> Result<&mut Project, DataError> {
        self.projects
            .iter_mut()
            .find(|project| project.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::Project, id))
    }

    fn project_or_last_mut(&mut self, id: Option<u64>) -> Result<&mut Project, DataError> {
        if let Some(id) = id {
            self.project_by_id_mut(id)
        } else {
            self.projects
                .last_mut()
                .ok_or_else(|| DataError::no_entities(Entity::Project))
        }
    }

    pub fn mr(&self, id: u64) -> Result<&MergeRequest, DataError> {
        self.merge_requests
            .iter()
            .find(|mr| mr.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::MergeRequest, id))
    }

    fn mr_mut(&mut self, id: u64) -> Result<&mut MergeRequest, DataError> {
        self.merge_requests
            .iter_mut()
            .find(|mr| mr.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::MergeRequest, id))
    }

    fn mr_or_last(&self, id: Option<u64>) -> Result<&MergeRequest, DataError> {
        if let Some(id) = id {
            self.mr(id)
        } else {
            self.merge_requests
                .last()
                .ok_or_else(|| DataError::no_entities(Entity::MergeRequest))
        }
    }

    fn mr_or_last_mut(&mut self, id: Option<u64>) -> Result<&mut MergeRequest, DataError> {
        if let Some(id) = id {
            self.mr_mut(id)
        } else {
            self.merge_requests
                .last_mut()
                .ok_or_else(|| DataError::no_entities(Entity::MergeRequest))
        }
    }

    pub fn statuses(
        &self,
        project: u64,
        commit: &CommitId,
    ) -> Result<Vec<&CommitStatus>, DataError> {
        self.project_by_id(project).map(|_| {
            self.statuses
                .iter()
                .filter(|status| &status.commit == commit && status.project == project)
                .collect()
        })
    }

    pub fn mr_comments(&self, id: u64) -> Result<Vec<&Comment>, DataError> {
        self.mr(id).map(|mr| {
            mr.comments
                .iter()
                .map(|&comment_id| {
                    self.comment(comment_id)
                        .expect("a non-existent comment is referenced")
                })
                .collect()
        })
    }

    pub fn mr_awards(&self, id: u64) -> Result<Vec<&Award>, DataError> {
        self.mr(id).map(|mr| {
            mr.awards
                .iter()
                .map(|&award_id| {
                    self.award(award_id)
                        .expect("a non-existent award is referenced")
                })
                .collect()
        })
    }

    pub fn comment_awards(&self, id: u64) -> Result<Vec<&Award>, DataError> {
        self.comment(id).map(|comment| {
            comment
                .awards
                .iter()
                .map(|&award_id| {
                    self.award(award_id)
                        .expect("a non-existent award is referenced")
                })
                .collect()
        })
    }

    pub fn issue(&self, id: u64) -> Result<&Issue, DataError> {
        self.issues
            .iter()
            .find(|issue| issue.id == id)
            .ok_or_else(|| DataError::no_entities(Entity::Issue))
    }

    fn issue_mut(&mut self, id: u64) -> Result<&mut Issue, DataError> {
        self.issues
            .iter_mut()
            .find(|issue| issue.id == id)
            .ok_or_else(|| DataError::no_entities(Entity::Issue))
    }

    fn award(&self, id: u64) -> Result<&Award, DataError> {
        self.awards
            .iter()
            .find(|award| award.id == id)
            .ok_or_else(|| DataError::no_entities(Entity::Award))
    }

    fn comment(&self, id: u64) -> Result<&Comment, DataError> {
        self.comments
            .iter()
            .find(|comment| comment.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::Comment, id))
    }

    fn comment_mut(&mut self, id: u64) -> Result<&mut Comment, DataError> {
        self.comments
            .iter_mut()
            .find(|comment| comment.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::Comment, id))
    }

    fn comment_or_last_mut(&mut self, id: Option<u64>) -> Result<&mut Comment, DataError> {
        if let Some(id) = id {
            self.comment_mut(id)
        } else {
            self.comments
                .last_mut()
                .ok_or_else(|| DataError::no_entities(Entity::Comment))
        }
    }

    fn pipeline_mut(&mut self, id: u64) -> Result<&mut Pipeline, DataError> {
        self.pipelines
            .iter_mut()
            .find(|pipeline| pipeline.id == id)
            .ok_or_else(|| DataError::no_entity(Entity::Pipeline, id))
    }

    fn pipeline_or_last_mut(&mut self, id: Option<u64>) -> Result<&mut Pipeline, DataError> {
        if let Some(id) = id {
            self.pipeline_mut(id)
        } else {
            self.pipelines
                .last_mut()
                .ok_or_else(|| DataError::no_entities(Entity::Pipeline))
        }
    }

    pub fn pipelines_for_commit(&self, project: u64, commit: &CommitId) -> Vec<&Pipeline> {
        self.pipelines
            .iter()
            .filter(|pipeline| pipeline.project == project && &pipeline.commit == commit)
            .collect()
    }

    pub fn jobs_for_pipeline(&self, pipeline: u64) -> Vec<&Job> {
        self.jobs
            .iter()
            .filter(|job| job.pipeline == pipeline)
            .collect()
    }

    pub fn last_pipeline_for_commit(&self, project: u64, commit: &CommitId) -> Option<u64> {
        self.pipelines
            .iter()
            .filter(|pipeline| pipeline.project == project && &pipeline.commit == commit)
            .map(|pipeline| pipeline.id)
            .max()
    }

    pub fn handle_push<P>(
        &mut self,
        user: u64,
        project: P,
        update: &RefUpdate,
    ) -> Result<(), HookError>
    where
        P: AsRef<str>,
    {
        self.handle_push_impl(user, project.as_ref(), update)
    }

    #[allow(clippy::needless_collect)] // the `collect` is needed for lifetime purposes
    fn handle_push_impl(
        &mut self,
        user: u64,
        name: &str,
        update: &RefUpdate,
    ) -> Result<(), HookError> {
        let (project_id, project_url) = {
            let project = self.project(name)?;

            self.push_hook(project, user, update)
                .map_err(|err| HookError::create(Hook::Push, err))?;

            (project.id, project.url.clone())
        };

        const HEADS_PREFIX: &str = "refs/heads/";
        if update.ref_.starts_with(HEADS_PREFIX) {
            let branch = &update.ref_[HEADS_PREFIX.len()..];
            let num_comments = self.comments.len();

            let comments = self
                .merge_requests
                .iter_mut()
                .filter(|mr| mr.source_project == project_id && mr.source_branch == branch)
                .enumerate()
                .map(|(i, mr)| {
                    let old_commit = mem::replace(&mut mr.commit, CommitId::new(&update.new));
                    mr.old_commit = Some(old_commit);

                    if update.new == "0000000000000000000000000000000000000000"
                        && mr.state == hooks::MergeRequestState::Open
                    {
                        mr.state = hooks::MergeRequestState::Closed;
                    }

                    let new_id = num_comments + i;
                    mr.comments.push(new_id as u64);
                    let comment = Comment {
                        id: new_id as u64,
                        is_branch_update: true,
                        created_at: Utc::now(),
                        author: user,
                        content: String::new(),

                        awards: Vec::new(),
                    };

                    (mr.id, comment)
                })
                .collect::<Vec<_>>();
            comments
                .into_iter()
                .map(|(mr_id, comment)| {
                    self.comments.push(comment);

                    let mr = self.mr(mr_id)?;
                    self.mr_hook(mr, hooks::MergeRequestAction::Update)
                        .map_err(|err| HookError::create(Hook::MergeRequest, err))?;

                    Ok(())
                })
                .collect::<Result<Vec<_>, HookError>>()?;

            let ctx = GitContext::new(project_url);

            let merged_mrs = self
                .merge_requests
                .iter_mut()
                .filter(|mr| mr.target_project == project_id && mr.target_branch == branch)
                .map(|mr| {
                    let merge_base = ctx
                        .git()
                        .arg("merge-base")
                        .arg("--is-ancestor")
                        .arg(mr.commit.as_str())
                        .arg(&update.new)
                        .output()
                        .map_err(|err| GitError::subcommand("merge-base", err))?;
                    if !merge_base.status.success() {
                        if let Some(1) = merge_base.status.code() {
                            // Not an ancestor.
                            Ok(None)
                        } else {
                            Err(HookError::check_if_merged(&merge_base.stderr))
                        }
                    } else {
                        mr.state = hooks::MergeRequestState::Merged;
                        Ok(Some(mr.id))
                    }
                })
                .collect::<Result<Vec<_>, HookError>>()?;
            merged_mrs
                .into_iter()
                .map(|mr_id| {
                    let mr = self.mr_or_last(mr_id)?;
                    self.mr_hook(mr, hooks::MergeRequestAction::Merge)
                        .map_err(|err| HookError::create(Hook::MergeRequest, err))?;

                    Ok(())
                })
                .collect::<Result<Vec<_>, HookError>>()?;
        }

        Ok(())
    }

    pub fn exit(&self) -> Result<(), HookError> {
        self.drop_job("watchdog:exit", json!({}))
    }

    fn hook_user(&self, user: &User) -> hooks::User {
        hooks::User {
            id: user.id,
            handle: user.handle.clone(),
            name: user.name.clone(),
            email: user.email.clone(),
        }
    }

    fn hook_repo(&self, project: &Project) -> Result<hooks::Repo, DataError> {
        let parent = if let Some(parent_id) = project.parent {
            let parent = self.project_by_id(parent_id)?;

            Some(Box::new(self.hook_repo(parent)?))
        } else {
            None
        };

        Ok(hooks::Repo {
            id: project.id,
            name: project.name.clone(),
            url: project.url.clone(),
            forked_from: parent,
        })
    }

    fn hook_mr(&self, mr: &MergeRequest) -> Result<hooks::MergeRequest, DataError> {
        let source = self.project_by_id(mr.source_project)?;
        let target = self.project_by_id(mr.target_project)?;
        let author = self.user_by_id(mr.author)?;
        let hook_source = self.hook_repo(source)?;

        let hook_commit = |id: &CommitId| {
            hooks::Commit {
                repo: hook_source.clone(),
                refname: Some(mr.source_branch.clone()),
                id: id.as_str().into(),
                last_pipeline: self.last_pipeline_for_commit(source.id, id),
            }
        };

        Ok(hooks::MergeRequest {
            source_repo: hook_source.clone(),
            source_branch: mr.source_branch.clone(),
            target_repo: self.hook_repo(target)?,
            target_branch: mr.target_branch.clone(),
            id: mr.id,
            url: mr.url.clone(),
            work_in_progress: mr.work_in_progress,
            description: mr.description.clone(),
            commit: hook_commit(&mr.commit),
            old_commit: mr.old_commit.as_ref().map(hook_commit),
            author: self.hook_user(author),
            reference: mr.reference.clone(),
            remove_source_branch: mr.remove_source_branch,
        })
    }

    fn push_hook(&self, project: &Project, user: u64, update: &RefUpdate) -> Result<(), HookError> {
        let commit = hooks::Commit {
            repo: self.hook_repo(project)?,
            refname: Some(update.ref_.clone()),
            id: update.new.clone(),
            last_pipeline: None,
        };
        let author = self.user_by_id(user)?;

        let hook = hooks::PushHook {
            commit,
            author: self.hook_user(author),
            date: Utc::now(),
        };
        self.drop_job("push", hook)
    }

    fn project_hook(&self, project: &Project) -> Result<(), HookError> {
        let hook = hooks::ProjectHook {
            project: self.hook_repo(project)?,
        };
        self.drop_job("project", hook)
    }

    fn membership_hook(
        &self,
        project: &Project,
        user: u64,
        level: AccessLevel,
        action: hooks::MembershipAction,
    ) -> Result<(), HookError> {
        let user = self.user_by_id(user)?;

        let hook = hooks::MembershipHook {
            action,
            repo: self.hook_repo(project)?,
            user: self.hook_user(user),
            access: level.into(),
        };
        self.drop_job("membership", hook)
    }

    fn mr_hook(
        &self,
        mr: &MergeRequest,
        action: hooks::MergeRequestAction,
    ) -> Result<(), HookError> {
        let note = if action == hooks::MergeRequestAction::Comment {
            let comment_id = *mr
                .comments
                .last()
                .ok_or_else(|| DataError::no_entities(Entity::MergeRequestComment))?;
            let comment = self.comment(comment_id)?;
            let author = self.user_by_id(comment.author)?;

            Some(hooks::Note {
                id: comment.id,
                is_branch_update: comment.is_branch_update,
                created_at: comment.created_at,
                author: self.hook_user(author),
                content: comment.content.clone(),
            })
        } else {
            None
        };

        let hook = hooks::MergeRequestHook {
            state: mr.state,
            action,
            updated_at: Utc::now(),
            merge_request: self.hook_mr(mr)?,
            note,
        };
        self.drop_job("merge_request", hook)
    }

    fn drop_job<T>(&self, kind: &str, data: T) -> Result<(), HookError>
    where
        T: Serialize,
    {
        let json_data =
            serde_json::to_value(data).map_err(|err| HookError::serialize(kind.into(), err))?;
        self.queue_job(kind, json_data)
    }

    pub fn queue_job(&self, kind: &str, data: Value) -> Result<(), HookError> {
        let job_path = self
            .webhook_dir
            .join(format!("{}-{}.json", Utc::now().to_rfc3339(), kind));
        let job = json!({
            "kind": kind,
            "data": data,
        });

        let mut file =
            File::create(&job_path).map_err(|err| HookError::create_job(job_path.clone(), err))?;
        serde_json::to_writer(&mut file, &job).map_err(|err| HookError::write(job_path, err))
    }
}
