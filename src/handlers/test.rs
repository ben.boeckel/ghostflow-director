// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Test hosting service
//!
//! This module contains an implementation of a simple hosting service which runs in-process so
//! that unit tests for the logic within the director may be written instead of requiring a
//! complete hosting service solution.
//!
//! The general idea is that the service has a few lines of communication:
//!
//!   - the initial setup;
//!   - an internal communication channel to communicate via a `HostingService` instance; and
//!   - a socket on the filesystem so that Git hooks for the on-disk repositories may trigger
//!     actions on the service.

mod action;
mod data;
mod ghostflow;
mod handler;
pub mod hooks;
mod service;

pub use self::action::Action;
pub use self::handler::TestHandler;
pub use self::service::{EndSignal, TestService};
pub use crate::ghostflow_ext::AccessLevel;
#[rustfmt::skip]
pub use ::ghostflow::host::CommitStatusState;
