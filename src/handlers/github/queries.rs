// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// We are ignoring warnings about `GraphQLQuery` not being used in this module.
#![allow(unused_imports)]
// The GraphQL uses names we need to match.
#![allow(clippy::upper_case_acronyms)]

use chrono::Utc;
use ghostflow_github::RateLimitInfo;
use graphql_client::GraphQLQuery;

type DateTime = chrono::DateTime<Utc>;
type GitObjectID = String;

#[rustfmt::skip]
macro_rules! gql_query_base {
    ($name:ident) => {
        #[derive(GraphQLQuery)]
        #[graphql(
            schema_path = "src/handlers/github/graphql/schema.graphql",
            query_path = "src/handlers/github/graphql/query.graphql",
            deprecated = "warn",
            response_derives = "Debug, Clone",
            variables_derives = "Debug, Clone"
        )]
        pub struct $name;
    };
}

macro_rules! gql_query {
    ($name:ident, $query_name:expr) => {
        gql_query_base!($name);

        impl $name {
            pub(crate) fn name() -> &'static str {
                $query_name
            }
        }
    };
}

gql_query!(CommitID, "CommitID");
gql_query!(Members, "Members");
gql_query!(PullRequestInfo, "PullRequestInfo");

macro_rules! impl_into_rate_limit_info {
    ($type:path) => {
        impl From<$type> for RateLimitInfo {
            fn from(info: $type) -> Self {
                Self {
                    cost: info.cost,
                    limit: info.limit,
                    remaining: info.remaining,
                    reset_at: info.reset_at,
                }
            }
        }
    };
}

impl_into_rate_limit_info!(commit_id::RateLimitInfoRateLimit);
impl_into_rate_limit_info!(members::RateLimitInfoRateLimit);
impl_into_rate_limit_info!(pull_request_info::RateLimitInfoRateLimit);
