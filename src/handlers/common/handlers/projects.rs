// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use ghostflow::host::{HostingServiceError, Repo};
use json_job_dispatch::JobResult;
use serde_json::Value;
use thiserror::Error;

use crate::config::Host;

#[derive(Debug, Error)]
enum ProjectCreationError {
    #[error("failed to add webhook to {}: {}", project, source)]
    AddWebHook {
        project: String,
        #[source]
        source: HostingServiceError,
    },
    #[error("failed to add member to {}: {}", project, source)]
    AddMember {
        project: String,
        #[source]
        source: HostingServiceError,
    },
}

struct ProjectCreationResult;

impl ProjectCreationResult {
    fn add_webhook(project: String, source: HostingServiceError) -> JobResult {
        JobResult::fail(ProjectCreationError::AddWebHook {
            project,
            source,
        })
    }

    fn add_member(project: String, source: HostingServiceError) -> JobResult {
        JobResult::fail(ProjectCreationError::AddMember {
            project,
            source,
        })
    }

    fn nothing_to_do() -> JobResult {
        JobResult::reject("nothing to do")
    }
}

/// Handle the creation of a project on the service.
pub fn handle_project_creation(_: &Value, host: &Host, project: &Repo) -> JobResult {
    let webhook_result = host.webhook_url.as_ref().map(|url| {
        host.service
            .add_hook(&project.name, url)
            .map(|()| JobResult::Accept)
            .unwrap_or_else(|err| ProjectCreationResult::add_webhook(project.name.clone(), err))
    });

    let membership_result = host.global_user.as_ref().map(|(user, access_level)| {
        host.service
            .add_member(&project.name, user, *access_level)
            .map(|()| JobResult::Accept)
            .unwrap_or_else(|err| ProjectCreationResult::add_member(project.name.clone(), err))
    });

    let all_results = vec![webhook_result, membership_result];

    all_results
        .into_iter()
        .flatten()
        .reduce(JobResult::combine)
        .unwrap_or_else(ProjectCreationResult::nothing_to_do)
}
