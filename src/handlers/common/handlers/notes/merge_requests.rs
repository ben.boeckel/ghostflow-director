// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use std::borrow::Cow;

use ghostflow::host::HostingServiceError;
use ghostflow::utils::TrailerRef;
use itertools::Itertools;
use json_job_dispatch::JobResult;
use log::error;
use serde_json::Value;
use thiserror::Error;

use crate::actions;
use crate::actions::merge_requests::{self, Action, Data, Effect, Info};
use crate::config::Host;
use crate::handlers::common::data::MergeRequestNoteInfo;
use crate::handlers::common::handlers::notes::pairings::Pairings;
use crate::handlers::common::handlers::utils;

const HELP_COMMAND: &str = "help";

const CHECK_COMMAND: &str = "check";
const MERGE_COMMAND: &str = "merge";
const REFORMAT_COMMAND: &str = "reformat";
const STAGE_COMMAND: &str = "stage";
const TEST_COMMAND: &str = "test";
const UNSTAGE_COMMAND: &str = "unstage";

/// The table of known commands from their name to an implementation.
const KNOWN_COMMANDS: &[(&str, &dyn Action)] = &[
    (CHECK_COMMAND, &merge_requests::Check),
    (MERGE_COMMAND, &merge_requests::Merge),
    (REFORMAT_COMMAND, &merge_requests::Reformat),
    (STAGE_COMMAND, &merge_requests::Stage),
    (TEST_COMMAND, &merge_requests::Test),
    (UNSTAGE_COMMAND, &merge_requests::Unstage),
];

fn find_command(command: &str) -> Option<&'static dyn Action> {
    for (cmd, action) in KNOWN_COMMANDS {
        if command == *cmd {
            return Some(*action);
        }
    }

    None
}

#[derive(Debug, Clone, Copy)]
struct IncompatibleCommand {
    cmd1: &'static str,
    cmd2: &'static str,
    #[allow(dead_code)]
    // TODO(#87): The combinations need to be updated and use this reason.
    reason: &'static str,
}

impl IncompatibleCommand {
    const fn new(cmd1: &'static str, cmd2: &'static str, reason: &'static str) -> Self {
        Self {
            cmd1,
            cmd2,
            reason,
        }
    }

    fn check(self, cmd1: &str, cmd2: &str) -> Option<Self> {
        if self.cmd1 == cmd1 && self.cmd2 == cmd2 {
            Some(self)
        } else {
            None
        }
    }

    fn error_message_component(self) -> String {
        format!("{}` and `{}", self.cmd1, self.cmd2)
    }
}

/// The table of command names which are incompatible with each other (ordered).
const INCOMPATIBLE_TABLE: &[IncompatibleCommand] = &[
    IncompatibleCommand::new(CHECK_COMMAND, CHECK_COMMAND, "redundant"),
    IncompatibleCommand::new(CHECK_COMMAND, MERGE_COMMAND, "historical?"),
    IncompatibleCommand::new(CHECK_COMMAND, REFORMAT_COMMAND, "historical?"),
    IncompatibleCommand::new(CHECK_COMMAND, STAGE_COMMAND, "historical?"),
    // IncompatibleCommand::new(CHECK_COMMAND, TEST_COMMAND, "OK"),
    IncompatibleCommand::new(CHECK_COMMAND, UNSTAGE_COMMAND, "historical?"),
    //
    IncompatibleCommand::new(
        MERGE_COMMAND,
        CHECK_COMMAND,
        "merging requires a check anyways",
    ),
    IncompatibleCommand::new(MERGE_COMMAND, MERGE_COMMAND, "redundant"),
    IncompatibleCommand::new(
        MERGE_COMMAND,
        REFORMAT_COMMAND,
        "reformatting can change the content",
    ),
    IncompatibleCommand::new(
        MERGE_COMMAND,
        STAGE_COMMAND,
        "merging implicitly adds the topic to the stage",
    ),
    IncompatibleCommand::new(
        MERGE_COMMAND,
        TEST_COMMAND,
        "merge target branches are assumed to be tested",
    ),
    // IncompatibleCommand::new(MERGE_COMMAND, UNSTAGE_COMMAND, "OK"),
    //
    IncompatibleCommand::new(REFORMAT_COMMAND, CHECK_COMMAND, "historical?"),
    IncompatibleCommand::new(
        REFORMAT_COMMAND,
        MERGE_COMMAND,
        "reformatting can change the content",
    ),
    IncompatibleCommand::new(REFORMAT_COMMAND, REFORMAT_COMMAND, "redundant"),
    IncompatibleCommand::new(
        REFORMAT_COMMAND,
        STAGE_COMMAND,
        "reformatting can change the content",
    ),
    IncompatibleCommand::new(
        REFORMAT_COMMAND,
        TEST_COMMAND,
        "reformatting can change the content",
    ),
    IncompatibleCommand::new(REFORMAT_COMMAND, UNSTAGE_COMMAND, "historical?"),
    //
    IncompatibleCommand::new(
        STAGE_COMMAND,
        CHECK_COMMAND,
        "stage requires a check anyways",
    ),
    IncompatibleCommand::new(
        STAGE_COMMAND,
        MERGE_COMMAND,
        "merging implicitly adds the topic to the stage",
    ),
    IncompatibleCommand::new(
        STAGE_COMMAND,
        REFORMAT_COMMAND,
        "reformatting can change the content",
    ),
    IncompatibleCommand::new(STAGE_COMMAND, STAGE_COMMAND, "redundant"),
    // IncompatibleCommand::new(STAGE_COMMAND, TEST_COMMAND, "OK"),
    IncompatibleCommand::new(STAGE_COMMAND, UNSTAGE_COMMAND, "redundant"),
    //
    // IncompatibleCommand::new(TEST_COMMAND, CHECK_COMMAND, "OK"),
    IncompatibleCommand::new(
        TEST_COMMAND,
        MERGE_COMMAND,
        "merge target branches are assumed to be tested",
    ),
    IncompatibleCommand::new(
        TEST_COMMAND,
        REFORMAT_COMMAND,
        "reformatting can change the content",
    ),
    // IncompatibleCommand::new(TEST_COMMAND, STAGE_COMMAND, "OK"),
    // IncompatibleCommand::new(TEST_COMMAND, TEST_COMMAND, "OK"),
    // IncompatibleCommand::new(TEST_COMMAND, UNSTAGE_COMMAND, "OK"),
    //
    IncompatibleCommand::new(UNSTAGE_COMMAND, CHECK_COMMAND, "historical?"),
    // IncompatibleCommand::new(UNSTAGE_COMMAND, MERGE_COMMAND, "OK"),
    IncompatibleCommand::new(UNSTAGE_COMMAND, REFORMAT_COMMAND, "historical?"),
    IncompatibleCommand::new(UNSTAGE_COMMAND, STAGE_COMMAND, "historical"),
    // IncompatibleCommand::new(UNSTAGE_COMMAND, TEST_COMMAND, "OK"),
    IncompatibleCommand::new(UNSTAGE_COMMAND, UNSTAGE_COMMAND, "redundant"),
];

#[derive(Debug, Error)]
enum MergeRequestNoteError {
    #[error(
        "failed to get the access level for note author {}: {}",
        author,
        source
    )]
    NoAccessLevel {
        author: String,
        #[source]
        source: HostingServiceError,
    },
    #[error("failed to fetch source for {}: {}", url, source)]
    Fetch {
        url: String,
        #[source]
        source: HostingServiceError,
    },
}

struct NoteResult;

impl NoteResult {
    fn closed_merge_request(branch: &str) -> JobResult {
        JobResult::Reject(format!(
            "the merge request for the branch {} has been closed",
            branch,
        ))
    }

    fn already_handled() -> JobResult {
        JobResult::reject("this note has already been handled")
    }

    fn no_trailers() -> JobResult {
        JobResult::reject("no trailers present")
    }

    fn no_access_level(author: String, source: HostingServiceError) -> JobResult {
        JobResult::fail(MergeRequestNoteError::NoAccessLevel {
            author,
            source,
        })
    }

    fn no_commands() -> JobResult {
        JobResult::reject("no commands present")
    }

    fn fetch_mr(url: String, source: HostingServiceError) -> JobResult {
        JobResult::fail(MergeRequestNoteError::Fetch {
            url,
            source,
        })
    }
}

fn check_consistency<'a, C>(commands: C) -> Vec<IncompatibleCommand>
where
    C: Iterator<Item = &'a str> + Clone,
{
    Pairings::new(commands)
        .filter_map(|(cmd1, cmd2)| {
            INCOMPATIBLE_TABLE
                .iter()
                .filter_map(|icmd| icmd.check(cmd1, cmd2))
                .next() // There's only one to worry about anyways.
        })
        .collect()
}

/// Handle a note on a merge request.
pub fn handle_merge_request_note(
    data: &Value,
    host: &Host,
    mr_note: &MergeRequestNoteInfo,
    can_defer: bool,
) -> JobResult {
    let mr = &mr_note.merge_request;
    let note = &mr_note.note;
    let award_name = host.service.comment_award_name();

    if !mr.is_open {
        return NoteResult::closed_merge_request(&mr.merge_request.source_branch);
    }

    let already_handled = match host.service.get_mr_comment_awards(&mr.merge_request, note) {
        Ok(awards) => {
            awards.into_iter().any(|award| {
                award.name == award_name
                    && award.author.handle == host.service.service_user().handle
            })
        },
        Err(err) => {
            error!(
                target: "ghostflow-director/handler",
                "failed to get the awards for a note at {}: {:?}",
                mr.merge_request.url,
                err,
            );

            false
        },
    };

    if already_handled {
        return NoteResult::already_handled();
    }

    let (project, branch) = try_action!(utils::get_branch(
        host,
        &mr.merge_request.target_repo.name,
        &mr.merge_request.target_branch,
    ));

    // Parse trailers.
    let trailers = TrailerRef::extract(&mr_note.note.content);
    if trailers.is_empty() {
        return NoteResult::no_trailers();
    }

    // Parse commands from the trailers.
    let commands = trailers
        .iter()
        .filter_map(|trailer| {
            if matches!(trailer.token, "do" | "Do") {
                // TODO: handle quoting
                let mut values = trailer.value.split_whitespace();
                let command = values
                    .next()
                    .expect("the regular expression should require a non-whitespace value");
                Some((command, values.collect::<Vec<_>>()))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();
    if commands.is_empty() {
        return NoteResult::no_commands();
    }

    // Get the access level of the author.
    let access = match project.access_level(&note.author) {
        Ok(access) => access,
        Err(err) => return NoteResult::no_access_level(note.author.handle.clone(), err),
    };

    // State variables.
    let mut defer = false;
    let mut messages = Vec::new();
    let mut errors = Vec::new();

    // Check for action consistency.
    let consistency_errors = check_consistency(commands.iter().map(|(cmd, _)| *cmd));
    if consistency_errors.is_empty() {
        // Ensure the MR's topic is available locally.
        let fetch_res = host.service.fetch_mr(&project.context, &mr.merge_request);
        if let Err(err) = fetch_res {
            return NoteResult::fetch_mr(mr.merge_request.url.clone(), err);
        }

        // Build up data structures for the actions.
        let action_data = actions::Data {
            project,
            job_data: data,
            cause: actions::Cause {
                when: note.created_at,
                who: note.author.clone(),
                how: access,
                is_submitter: mr.merge_request.author.handle == note.author.handle,
            },
        };
        let mr_data = Data {
            action_data,
            main_branch: branch,
            info: Info {
                merge_request: Cow::Borrowed(&mr.merge_request),
                was_opened: mr.was_opened,
                was_merged: mr.was_merged,
                is_open: mr.is_open,
                date: mr.date,
            },
        };

        for (command, args) in commands {
            // Handle the `help` action here.
            let help_action = merge_requests::Help::new(KNOWN_COMMANDS);

            // We need a way to neuter command effects from within the test suite in order to avoid
            // all the other effects that occur when handling commands. This "command" bails out
            // after all of the sanity checking but before actually performing any logic while
            // leaving behind evidence of its behavior for detection by the test suite.
            #[cfg(test)]
            if command == "__test_mock" {
                messages.push("test mock sentinel".into());
                break;
            }

            let action = if command == HELP_COMMAND {
                &help_action
            } else if let Some(action) = find_command(command) {
                action
            } else {
                errors.push(format!(
                    "@{}: the `{}` command is not recognized at all.",
                    note.author.handle, command,
                ));
                continue;
            };

            let effects = action.perform(&args, &mr_data).into_iter();
            for effect in effects {
                match effect {
                    Effect::Message {
                        message,
                    } => {
                        // Avoid the intro for `help`.
                        if command == HELP_COMMAND {
                            messages.push(message);
                        } else {
                            messages.push(format!(
                                "While processing the `{}` command: {}",
                                command, message,
                            ));
                        }
                    },
                    Effect::Error {
                        message,
                    } => {
                        errors.push(format!(
                            "While processing the `{}` command: {}",
                            command, message,
                        ));
                    },
                    Effect::ActionError {
                        error,
                    } => {
                        errors.push(format!(
                            "While processing the `{}` command: {}",
                            command, error,
                        ));
                    },
                    Effect::DeferHandling => {
                        defer = true;
                    },
                    Effect::StopProcessing => {
                        defer = false;
                        break;
                    },
                }
            }
        }
    } else {
        let explanation = consistency_errors
            .into_iter()
            .map(IncompatibleCommand::error_message_component)
            .join("`; `");
        let consistency_msg = format!(
            "@{}: inconsistent command request: `{}`.",
            note.author.handle, explanation,
        );
        errors.push(consistency_msg);
    }

    if !defer {
        // Award the command comment to indicate that we handled it.
        if let Err(err) = host
            .service
            .award_mr_comment(&mr.merge_request, note, award_name)
        {
            error!(
                target: "ghostflow-director/handler",
                "failed to mark the comment on {} as seen: {:?}",
                mr.merge_request.url,
                err,
            );

            errors.push(format!("Failed to mark the comment as seen: `{}`.", err));
        }
    }

    utils::handle_result(&messages, &errors, defer && can_defer, |comment| {
        host.service.post_mr_comment(&mr.merge_request, comment)
    })
}

#[cfg(test)]
mod tests {
    use serde_json::{json, Value};

    use crate::handlers::test::*;

    fn test_basic_config() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {},
                },
            },
        })
    }

    fn test_full_config() -> Value {
        json!({
            "ghostflow/example": {
                "branches": {
                    "master": {
                        "reformat": {
                            "required_access_level": "developer",
                            "formatters": [],
                        },
                    },
                },
            },
        })
    }

    const MERGE_REQUEST_TOPIC: &str = "6f781d4d4d85dfd5a609532a26bcc6fd63fcef51";

    fn test_command_impl(config: Value, test_name: &str, command: &str, comment: String) {
        let mut service = TestService::new(
            test_name,
            [
                Action::create_user("fork"),
                Action::new_project("ghostflow", "example"),
                Action::fork_project("ghostflow", "example", "fork"),
                Action::push("fork", "example", "mr-source", MERGE_REQUEST_TOPIC),
                Action::create_mr("ghostflow", "example", "fork", "", false),
                Action::mr_comment("ghostflow", command),
            ],
        )
        .unwrap();
        let end = EndSignal::MergeRequestComment {
            content: comment,
        };

        service.launch(config, end).unwrap();
    }

    fn test_command<N, X, C>(test_name: N, command: X, comment: C)
    where
        N: AsRef<str>,
        X: AsRef<str>,
        C: Into<String>,
    {
        test_command_impl(
            test_basic_config(),
            test_name.as_ref(),
            command.as_ref(),
            comment.into(),
        )
    }

    fn test_command_full<N, X, C>(test_name: N, command: X, comment: C)
    where
        N: AsRef<str>,
        X: AsRef<str>,
        C: Into<String>,
    {
        test_command_impl(
            test_full_config(),
            test_name.as_ref(),
            command.as_ref(),
            comment.into(),
        )
    }

    #[test]
    fn test_unrecognized_commands() {
        let expect = "Errors:\n\n  - @ghostflow: the `bogus` command is not recognized at all.";
        test_command("test_unknown_commands(Do)", "Do: bogus", expect);
        test_command("test_unknown_commands(do)", "do: bogus", expect);
    }

    #[test]
    fn test_always_supported_commands() {
        use super::*;

        let expect = "Messages:\n\n  - test mock sentinel";
        let always_supported = [HELP_COMMAND, CHECK_COMMAND];

        for command in always_supported.iter() {
            let test_name = format!("test_always_supported_commands({})", command);
            let command = format!("Do: __test_mock\nDo: {}", command);
            test_command(test_name, command, expect);
        }
    }

    #[test]
    fn test_command_consistency() {
        use super::*;

        let ok = |cmd1, cmd2| -> (&str, &str, Cow<str>) {
            (cmd1, cmd2, "Messages:\n\n  - test mock sentinel".into())
        };
        let conflict = |cmd1, cmd2| -> (&str, &str, Cow<str>) {
            (
                cmd1,
                cmd2,
                format!(
                    "Errors:\n\n  - @ghostflow: inconsistent command request: `{}` and `{}`.",
                    cmd1, cmd2,
                )
                .into(),
            )
        };

        let combinations = [
            ok(HELP_COMMAND, HELP_COMMAND),
            ok(HELP_COMMAND, CHECK_COMMAND),
            ok(HELP_COMMAND, MERGE_COMMAND),
            ok(HELP_COMMAND, REFORMAT_COMMAND),
            ok(HELP_COMMAND, STAGE_COMMAND),
            ok(HELP_COMMAND, TEST_COMMAND),
            ok(HELP_COMMAND, UNSTAGE_COMMAND),
            //
            ok(CHECK_COMMAND, HELP_COMMAND),
            conflict(CHECK_COMMAND, CHECK_COMMAND),
            conflict(CHECK_COMMAND, MERGE_COMMAND),
            conflict(CHECK_COMMAND, REFORMAT_COMMAND),
            conflict(CHECK_COMMAND, STAGE_COMMAND),
            ok(CHECK_COMMAND, TEST_COMMAND),
            conflict(CHECK_COMMAND, UNSTAGE_COMMAND),
            //
            ok(MERGE_COMMAND, HELP_COMMAND),
            conflict(MERGE_COMMAND, CHECK_COMMAND),
            conflict(MERGE_COMMAND, MERGE_COMMAND),
            conflict(MERGE_COMMAND, REFORMAT_COMMAND),
            conflict(MERGE_COMMAND, STAGE_COMMAND),
            conflict(MERGE_COMMAND, TEST_COMMAND),
            ok(MERGE_COMMAND, UNSTAGE_COMMAND),
            //
            ok(REFORMAT_COMMAND, HELP_COMMAND),
            conflict(REFORMAT_COMMAND, CHECK_COMMAND),
            conflict(REFORMAT_COMMAND, MERGE_COMMAND),
            conflict(REFORMAT_COMMAND, REFORMAT_COMMAND),
            conflict(REFORMAT_COMMAND, STAGE_COMMAND),
            conflict(REFORMAT_COMMAND, TEST_COMMAND),
            conflict(REFORMAT_COMMAND, UNSTAGE_COMMAND),
            //
            ok(STAGE_COMMAND, HELP_COMMAND),
            conflict(STAGE_COMMAND, CHECK_COMMAND),
            conflict(STAGE_COMMAND, MERGE_COMMAND),
            conflict(STAGE_COMMAND, REFORMAT_COMMAND),
            conflict(STAGE_COMMAND, STAGE_COMMAND),
            ok(STAGE_COMMAND, TEST_COMMAND),
            conflict(STAGE_COMMAND, UNSTAGE_COMMAND),
            //
            ok(TEST_COMMAND, HELP_COMMAND),
            ok(TEST_COMMAND, CHECK_COMMAND),
            conflict(TEST_COMMAND, MERGE_COMMAND),
            conflict(TEST_COMMAND, REFORMAT_COMMAND),
            ok(TEST_COMMAND, STAGE_COMMAND),
            ok(TEST_COMMAND, TEST_COMMAND),
            ok(TEST_COMMAND, UNSTAGE_COMMAND),
            //
            ok(UNSTAGE_COMMAND, HELP_COMMAND),
            ok(UNSTAGE_COMMAND, MERGE_COMMAND),
            conflict(UNSTAGE_COMMAND, CHECK_COMMAND),
            conflict(UNSTAGE_COMMAND, REFORMAT_COMMAND),
            conflict(UNSTAGE_COMMAND, STAGE_COMMAND),
            ok(UNSTAGE_COMMAND, TEST_COMMAND),
            conflict(UNSTAGE_COMMAND, UNSTAGE_COMMAND),
        ];

        for (cmd1, cmd2, comment) in combinations.iter() {
            let test_name = format!("test_command_consistency({},{})", cmd1, cmd2);
            let command = format!("Do: __test_mock\nDo: {}\nDo: {}", cmd1, cmd2);
            test_command_full(test_name, command, comment.as_ref());
        }
    }
}
