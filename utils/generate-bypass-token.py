#!/usr/bin/env python3
'''
Create a merge request bypass token.

This scripts generates a bypass token to allow a merge request to be merged
without considering the CI results.

The only required flags are:

  - `-s`: the secret for the project
  - `-p`: the project which hosts the merge request
  - `-m`: the ID of the merge request

Optional flags include:

  - `-g`: the GitLab instance to communicate with (defaults to
    `gitlab.kitware.com`)
  - `-t`: the GitLab token (required for private projects)
'''

import argparse
import hashlib
import requests


def options():
    p = argparse.ArgumentParser(
        description='Create a merge request for the release process')
    p.add_argument('-s', '--secret', type=str, required=True,
                   help='the secret for the project')
    p.add_argument('-g', '--gitlab', type=str, default='gitlab.kitware.com',
                   help='the gitlab instance to communicate with')
    p.add_argument('-p', '--project', type=str, required=True,
                   help='the project to generate a token for')
    p.add_argument('-m', '--merge-request', type=int, required=True,
                   help='the merge request to generate a token for')
    p.add_argument('-t', '--token', type=str,
                   help='API token to communicate for GitLab (for private projects)')
    return p


GHOSTFLOW_BYPASS_SALT = 'ghostflow-director-bypass-token'
GHOSTFLOW_BYPASS_PREFIX = 'gdbt-'


class Gitlab(object):
    def __init__(self, gitlab_url, token):
        self.gitlab_url = f'https://{gitlab_url}/api/v4'
        self.headers = {}
        if token is not None:
            headers['PRIVATE-TOKEN'] = token

    def get(self, endpoint, **kwargs):
        rsp = requests.get(f'{self.gitlab_url}/{endpoint}',
                           headers=self.headers,
                           params=kwargs)

        if rsp.status_code != 200:
            raise RuntimeError(f'Failed request to {endpoint}: {rsp.content}')

        return rsp.json()


if __name__ == '__main__':
    p = options()
    opts = p.parse_args()

    gitlab = Gitlab(opts.gitlab, opts.token)

    project_safe = opts.project.replace('/', '%2f')
    mr = gitlab.get(f'projects/{project_safe}/merge_requests/{opts.merge_request}')

    url = mr['web_url']
    commit = mr['sha']

    m = hashlib.sha256()
    m.update(GHOSTFLOW_BYPASS_SALT.encode('utf-8'))
    m.update(url.encode('utf-8'))
    m.update(commit.encode('utf-8'))
    m.update(opts.secret.encode('utf-8'))

    print(f'{GHOSTFLOW_BYPASS_PREFIX}{m.hexdigest()}')
