default_checks: &default_checks
    project_check_whitespace:
        kind: "check_whitespace"

default_stage: &default_stage
    required_access_level: "developer"

default_merge: &default_merge
    required_access_level: "developer"
    service_access_level: "maintainer"

default_reformat: &default_reformat
    required_access_level: "developer"
    formatters: []

default_test_jobs: &default_test_jobs
    required_access_level: "developer"
    backend: "jobs"
    config:
        queue: "TEST_JOB_QUEUE"
        test_updates: true
        help_string: "The `test` action drops a test job file for processing by the testing infrastructure."

workdir: "WORKDIR"
queue: "QUEUE"
hosts:
    gitlab:
        host_api: "gitlab"
        host_url: "GITLAB_HOST"
        secrets_path: "SECRETS"
        webhook_url: "http://HOSTNAME:50000/gitlab"
        maintainers:
            - "@maintainer"
        projects:
            origin/example:
                unprotect_source_branches: true
                submodules:
                    relative: "origin/submodule"
                    absolute: "WORKDIR"
                checks: *default_checks
                data:
                    destinations:
                        - "WORKDIR/data"
                commit_dashboards:
                    - status_name: "dashboard-for-{branch_name}"
                      description: "Dashboard status for {branch_name}"
                      url: "https://example.com/{commit}"
                merge_request_dashboards:
                    - status_name: "dashboard-for-{target_branch}"
                      description: "Dashboard status for {target_branch}"
                      url: "https://example.com/{commit}"
                branches:
                    master:
                        issue_labels:
                            - "closed-by-ghostflow"
                        stage:
                            quiet: true
                            required_access_level: "maintainer"
                            update_policy: "unstage"
                        merge:
                            quiet: true
                            required_access_level: "maintainer"
                            service_access_level: "maintainer"
                            log_limit: 1
                        test:
                            required_access_level: "maintainer"
                            backend: "refs"
                            config:
                                quiet: true
                        reformat: *default_reformat
                    release:
                        merge:
                            quiet: true
                            required_access_level: "maintainer"
                            required_access_level_ff: "maintainer"
                            service_access_level: "maintainer"
                        into_branches:
                            - master
                    next:
                        issue_labels:
                            - "closed-by-ghostflow"
                        stage:
                            required_access_level: "developer"
                            update_policy: "ignore"
                        merge:
                            required_access_level: "developer"
                            service_access_level: "maintainer"
                        test: *default_test_jobs
                        reformat: *default_reformat
                    pu:
                        stage:
                            required_access_level: "developer"
                            update_policy: "restage"
                        merge: *default_merge
                        test: *default_test_jobs
                        reformat: *default_reformat
