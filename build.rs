// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

// XXX(rust-1.66)]
#![allow(clippy::uninlined_format_args)]

use std::env;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::process::{Command, Output};

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join("git_info.rs");
    let mut f = File::create(dest_path).unwrap();

    let git_version_suffix = if let Some(hash) = git_head_sha1() {
        format!(
            " ({}{})",
            hash,
            if let Some(false) = git_tree_is_clean() {
                " worktree dirty"
            } else {
                ""
            },
        )
    } else {
        String::new()
    };

    writeln!(
        f,
        "\
        const VERSION_STRING: &str = concat!(\n    \
            clap::crate_version!(),\n    \
            {:?},\n\
        );",
        git_version_suffix,
    )
    .unwrap();

    if let Some(git_paths) = git_tracked_paths() {
        for entry in git_paths {
            println!("cargo:rerun-if-changed={}", entry.display());
        }
    }
}

fn run_git_command(args: &[&str]) -> Option<Output> {
    Command::new("git").args(args).output().ok().and_then(|o| {
        if o.status.success() {
            Some(o)
        } else {
            None
        }
    })
}

fn git_head_sha1() -> Option<String> {
    run_git_command(&["rev-parse", "--short", "HEAD"])
        .and_then(|o| String::from_utf8(o.stdout).ok())
        .map(|mut s| {
            let len = s.trim_end().len();
            s.truncate(len);
            s
        })
}

fn git_tree_is_clean() -> Option<bool> {
    run_git_command(&["status", "--porcelain", "--untracked-files=no"]).map(|o| o.stdout.is_empty())
}

fn git_tracked_paths() -> Option<Vec<PathBuf>> {
    let head_ref = run_git_command(&["rev-parse", "--git-path", "HEAD"])
        .map(|o| PathBuf::from(String::from_utf8_lossy(&o.stdout).into_owned()));
    run_git_command(&["ls-files"]).map(|o| {
        String::from_utf8_lossy(&o.stdout)
            .lines()
            .map(PathBuf::from)
            .chain(head_ref)
            .collect()
    })
}
